@all
@roles
@alternativeRoles
Feature: Alternative Role Managment
    As a Metahub user
    Wants to manage roles information 
    So I can create, edit and delete roles information

Background:
    Given I am logged in metahub with adm user
    And I am already in the Metahub roles page

Scenario: Trying create new role without role name
    When I click in the button "New Role"
    And I fill the text area Description with the value 'Description test'
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    Then I can see the field error message "Please enter a value."
    And I can see user page error message "Please correct entry field errors."

Scenario Outline: Trying create new role with invalid role name
    When I click in the button "New Role"
    And I fill the field Role Name with the value '<ROLENAME>'
    And I fill the text area Description with the value 'Description test'
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    Then I can see the field error message "Must be between 1-64 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter."
    And I can see user page error message "Please correct entry field errors."
Examples:
       | ROLENAME                                                          |
       | <>                                                                |
       | longteststring................................................... |

Scenario: Trying create new role with name already exists
    When I click in the button "New Role"
    And I fill the field Role Name with the value 'Manage Notifications'
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    Then I can see user page error message "Error: Unable to create/update role Manage Notifications: Proposed role name has already been used."

Scenario: Trying create new role without permissions
    When I click in the button "New Role"
    And I fill the field Role Name with the value 'test name'
    And I fill the text area Description with the value 'Description test'
    And I click in the button "Save"
    Then I can see the field error message "Please make selections"
    And I can see user page error message "Please correct entry field errors."

Scenario: Trying create new role with only User View permission
    When I click in the button "New Role"
    And I fill the field Role Name with the value 'test name'
    And I fill the text area Description with the value 'Description test'
    And I select the permission 'User View'
    And I click in the button "Save"
    Then I can see user page error message "Error: Unable to create new role test name: Missing permission dependencies: privilege User View requires Role View."

Scenario: Create a valid role for test edit role name without name
    When I click in the button "New Role"
    And I fill the field Role Name with the value 'test name'
    And I fill the text area Description with the value 'Description test'
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    And I filter the role by the Name 'test name'
    And I click in the option "Edit"
    And I fill the field Role Name with the value ' '
    And I click in the button "Save"
    Then I can see the field error message "Please enter a value."

Scenario Outline: trying edit role name with invalid role name
    When I filter the role by the Name 'test name'
    And I click in the option "Edit"
    And I fill the field Role Name with the value '<ROLENAME>'
    And I click in the button "Save"
    Then I can see the field error message "Must be between 1-64 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter."
Examples:
       | ROLENAME                                                          |
       | <>                                                                |
       | longteststring................................................... |

Scenario: trying edit role permissions to without permissions
    When I filter the role by the Name 'test name'
    And I click in the option "Edit"
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    Then I can see the field error message "Please make selections"

Scenario: trying edit role permissions with user view permission
    When I filter the role by the Name 'test name'
    And I click in the option "Edit"
    And I select the permission 'User View'
    And I click in the button "Save"
    Then I can see user page error message "Error: Unable to create new role test name: Missing permission dependencies: privilege User View requires Role View."

Scenario: Trying edit role name to name already exists and delete the role by api
    When I filter the role by the Name 'test name'
    And I click in the option "Edit"
    And I fill the field Role Name with the value 'Manage Notifications'
    And I click in the button "Save"
    Then I delete the test role by api
    And I can see user page error message "Error: Unable to create/update role Manage Notifications: Proposed role name has already been used."
