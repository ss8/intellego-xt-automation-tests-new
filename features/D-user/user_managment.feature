@all
@user
Feature: User Managment
    As a Metahub user
    Wants to manage user information 
    So I can create, edit and delete user information
@smoketest
Scenario: Creating new user with success
    Given I am logged in metahub with adm user
    And I am already in the Metahub users page
    When I click in the button "New User"
    And I fill the field User with the value "user_test"
    And I fill the field Email with the value "test@test.com"
    And I fill the field First Name with the value "user"
    And I fill the field Last Name with the value "name"
    And I fill the field Role with the values "Manage Users,Manage Roles,Manage Teams,Manage Audit Log,Manage Alarms,Manage Configuration,Manage Dashboards/Reports,Manage DataSource and Data Imports,Manage Query Permissions,Manage Idossier Permissions,Manage Area Permissions,Manage Notifications"
    And I fill the field Password with the value "Test123456!"
    And I click in the button "Save"
    Then I can see the user informations:
      |  USER_ID   |    EMAIL      | FIRST_NAME |  LAST_NAME  |    STATE     |
      | user_test  | test@test.com |    user    |     name    | new-password |

@smoketest
Scenario: Updating password on the first access
    Given I am already in the Metahub login page
    When I fill the field Login with "user_test" and Password with "Test123456!"
    And I click submit
    And I fill the field Enter Current Password with the value "Test123456!"
    And I fill the field Enter New Password
    And I fill the field Confirm New Password
    And I click in the button Submit
    Then I can see the Metahub Home Page
    And I click on the profile image with the initials "UN"
    And I can see the profile information "user name (user_test)"

Scenario: Force password change
   Given I am logged in metahub with adm user
   And I already have a user with "user_test" name
   And I filter the user by the name "user_test"
   When I click in the option "Edit"
   And I click in the button "Force Password Change"
   And I click in the button "Save"
   Then I can see the user informations:
      |  USER_ID   |    EMAIL      | FIRST_NAME |  LAST_NAME  |    STATE     |
      | user_test  | test@test.com |    user    |     name    | new-password |

Scenario: Edit and deactivate user with success
    Given I am logged in metahub with adm user
    And I already have a user with "user_test" name
    And I filter the user by the name "user_test"
    When I click in the option "Edit"
    And I fill the field First Name with the value "user_edited"
    And I fill the field Last Name with the value "name_edited"
    And I click in the button "Deactivate"
    And I click in the button "Save"
    Then I can see the user informations:
      |  USER_ID  |    EMAIL      | FIRST_NAME  |  LAST_NAME  |    STATE     |
      | user_test | test@test.com | user_edited | name_edited |  deactivated |

Scenario: Trying access with the deactivated user
    Given I am already in the Metahub login page
    When I fill the field Login with "user_test" and Password
    And I click submit 
    Then I can see the error message "Wrong password or username. Try again."

Scenario: Deleting user with success
    Given I am logged in metahub with adm user
    And I filter the user by the name "user_test"
    When I click in the option "Delete"
    And I click in the button "Delete"
    And I filter the user by the name "user_test"
    Then I cannot see a user with the name "user_test"
