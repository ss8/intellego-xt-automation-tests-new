@all
@teams
@alternativeTeams
Feature: Alternative team Managment
    As a Metahub user
    Wants to manage team information 
    So I can't create user information on invalid password

Background:
    Given I am logged in metahub with adm user

Scenario: Trying create new team without team name
    Given I am already in the Metahub teams page
    When I click in the button "New Team"
    And I fill the text area Description with the value 'Description test'
    And I select the user 'qa'
    And I click in the button 'Save'
    Then I can see the field error message "Please enter a value."

Scenario Outline: Trying create new team using invalid team name
    Given I am already in the Metahub teams page
    When I click in the button "New Team"
    And I fill the field team Name with the value "<TEAMNAME>"
    And I fill the text area Description with the value 'Description test'
    And I select the user 'qa'
    And I click in the button 'Save'
    Then I can see the field error message 'Must be between 1-64 characters. Can include letters, digits, spaces and the following special characters: -_.~!@#$%^*+?/. Please re-enter.'
Examples:
       | TEAMNAME                                                           |
       | <>                                                                 |
       | longteststring...................................................  |
       