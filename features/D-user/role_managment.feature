@all
@roles
Feature: Role Managment
    As a Metahub user
    Wants to manage roles information 
    So I can create, edit and delete roles information

Scenario: Creating a new role with success
    Given I am logged in metahub with adm user
    And I am already in the Metahub roles page
    When I click in the button "New Role"
    And I fill the field Role Name with the value 'Role_test'
    And I fill the text area Description with the value 'Description test'
    And I select the permission 'View iDossier'
    And I click in the button "Save"
    And I filter the role by the Name 'Role_test'
    Then I can see the role informations:
      |   NAME    |   DESCRIPTION    |  PERMISSIONS  |
      | Role_test | Description test | View iDossier |

Scenario: Inserting the new role to a user
    Given I already have created the user "user_test" by api
    And I am logged in metahub with adm user
    When I filter the user by the name "user_test"
    And I click in the option "Edit"
    And I fill the field Role with the values "Role_test"
    And I click in the button "Save"

Scenario: Checking permissions of the new user
    Given I am already in the Metahub login page
    When I fill the field Login with "user_test" and Password with "Test123456!"
    And I click submit
    And I fill the field Enter Current Password with the value "Test123456!"
    And I fill the field Enter New Password
    And I fill the field Confirm New Password
    And I click in the button Submit
    And I can see only the menu Idossier
    And I cannot see the button "New iDossier"

Scenario: Deleting role with success and deleting user by api
    Given I am logged in metahub with adm user
    And I am already in the Metahub roles page
    When I filter the role by the Name 'Role_test'
    And I click in the option "Delete"
    And I click in the button "Delete"
    Then I delete the user "user_test" by api