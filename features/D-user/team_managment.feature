@all
@teams
Feature: Team Managment
    As a Metahub user
    Wants to manage Team information 
    So I can create, edit and delete Team information

Background:
    Given I am logged in metahub with adm user

Scenario: Creating a new Team with success
    Given I am already in the Metahub teams page
    When I click in the button "New Team"
    And I fill the field team Name with the value 'Team_test'
    And I fill the text area Description with the value 'Description test'
    And I select the user 'qa'
    And I click in the button 'Save'
    Then I can see the teams informations:
       |   NAME    |   DESCRIPTION    | NUMBER_OF_MEMBERS | MEMBERS  |
       | Team_test | Description test |        1          |    qa    |

Scenario: Edit a team with sucess
    Given I am already in the Metahub teams page
    When I click in the option "Edit"
    And I fill the field team Name with the value 'Edited_team_test'
    And I fill the text area Description with the value 'Edited description test'
    And I click in the button 'Save'
    Then I can see the teams informations:
       |       NAME       |        DESCRIPTION      | NUMBER_OF_MEMBERS | MEMBERS  |
       | Edited_team_test | Edited description test |        1          |    qa    |

Scenario: Deleting role with success
    Given I am already in the Metahub teams page
    When I filter the role by the Name 'Team_test'
    And I click in the option "Delete"
    And I click in the button "Delete"