@all
@user
@alternativeUser
Feature: Alternative User Managment
    As a Metahub user
    Wants to manage user information 
    So I can't create user information on invalid password

Scenario Outline: Trying create new user using invalid password
    Given I am logged in metahub with adm user
    And I am already in the Metahub users page
    When I click in the button "New User"
    And I fill the field User with the value "user_test"
    And I fill the field Email with the value "test@test.com"
    And I fill the field First Name with the value "user"
    And I fill the field Last Name with the value "name"
    And I fill the field Role with the values "Manage Users"
    And I fill the field Password with the value "<PASSWORD>"
    And I click in the button "Save"
    Then I can see the error password message 'Error: Invalid password format. Password must have a minimum of 10 characters with at least 1 uppercase, 1 lowercase, 1 number and 1 symbol (!@#$%^&*()-_=+{}[]:.,?/|), and cannot be the same as the last 5 passwords.'
Examples:
       |   PASSWORD   |
       |  Test        |
       |  test123456! |
       |  TEST123456! |
       |  TestTestte! |
       |  Test1234567 |
