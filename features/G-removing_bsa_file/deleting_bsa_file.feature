@all
@datasource
@bsadatasource
@eventdatasource
@smoketest
Feature: Enrichment Data Source Managment
    As a Metahub user
    Wants to manage bsa information 
    So I can import a enrichment bsa data source

Background:
    Given I am logged in metahub

Scenario: Deleting BSA data imported
    Given I am already in the import page
    And I click in the "Enrichment Data Sources" menu
    And I already have a file with "BSA_DATA_SOURCE_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "BSA Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a BSA Data Source file with success
    Given I am already in the import page
    And I click in the "Enrichment Data Sources" menu
    And I already have a file with "BSA_DATA_SOURCE_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
