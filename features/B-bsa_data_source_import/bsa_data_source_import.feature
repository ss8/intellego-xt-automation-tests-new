@all
@datasource
@bsadatasource
@eventdatasource
@smoketest
Feature: Enrichment Data Source Managment
    As a Metahub user
    Wants to manage bsa information 
    So I can import a enrichment bsa data source

Background:
    Given I am logged in metahub

Scenario: Importing a new data source bsa file
    Given I am already in the import page
    When I click in the "Enrichment Data Sources" menu
    And I click in the "New Data Source" button
    And I fill the field Data Source Name with the value "BSA_DATA_SOURCE_FILE"
    And I fill the field Data Source Description with the value "BSA_DATA_SOURCE_FILE"
    And I select the option "BSA" in the Data Source Type field
    And I select the bsa file to be imported
    And I select the option "Cell Type" in the Mapped Field column at the line with value "Type"
    And I select the option "Cell Id" in the Mapped Field column at the line with value "ID"
    And I select the option "Azimuth" in the Mapped Field column at the line with value "azimuth"
    And I select the option "Width" in the Mapped Field column at the line with value "width"
    And I select the option "Range" in the Mapped Field column at the line with value "range"
    And I select the option "Valid From" in the Mapped Field column at the line with value "valid_from"
    And I select the option "Valid To" in the Mapped Field column at the line with value "valid_to"
    And I select the option "Physical Address" in the Mapped Field column at the line with value "address"
    And I select the option "Latitude" in the Map as column at the line with value "lat"
    And I select the option "Longitude" in the Map as column at the line with value "lon"
    And I select the checkbox of the fields "lat" and "lon" and click in "Merge Fields"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    And I can see the file information:
       |   DATA_SOURCE_NAME     |   FILE_TYPE           |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |  BSA_DATA_SOURCE_FILE  | BSA_DATA_SOURCE_FILE  |      qa      |       0         |        0         |      0         |

Scenario: Importing new data to a BSA Data Source
    Given I am already in the import page
    When I click in the "Enrichment Data Sources" menu
    And I already have a file with "BSA_DATA_SOURCE_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "BSA Data" value
    And I select the bsa file to be imported
    And I click in the button "Save"
    Then I can see the toast message "Data Source Import has been created successfully"
        And I can see the informations:
      |    Name     |  Status  | User  | Imported_File_Name                                                      | Total_Records   |
      |  BSA Data   | Ingested |  qa   | /var/enrichment/upload/BSA_DATA_SOURCE_FILE/Cell_VNP_v3_300.csv.watched |      294        |
