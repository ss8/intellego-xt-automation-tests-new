@all
@datasource
@bsadatasource
@jsondatasource
@anprdatasource
@eventdatasource
@xmldatasource
@importidossier
@nolocationdatasource
@smoketest
@roles
@teams
@user
@idossier
@login
@streamingdatasource
@autoingestdatasource
@analysis
@alternativeAnalysis
Feature: First access
    As a Metahub user
    Wants to change the password on the first access
    So I can access the metahub

Scenario: Update Password First access
    Given I am already in the Metahub login page
    When I fill the field Login with "qa" and Password with "Test123456!"
    And I click submit
    And I fill the field Enter Current Password with the value "Test123456!"
    And I fill the field Enter New Password
    And I fill the field Confirm New Password
    And I click in the button Submit
    Then I can see the Metahub Home Page
