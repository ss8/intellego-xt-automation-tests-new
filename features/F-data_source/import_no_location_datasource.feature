@all
@datasource
@nolocationdatasource
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can import event with no location

Background:
    Given I am logged in metahub

Scenario: Importing a new data source event file with no location
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "NO_LOCATION_FILE"
    And I select the option "Regular" in the Data Source Type field
    And I select the file with no location to be imported
    And I select the option "Unknown_Protocol" in the Event Type field
    And I select the option "Event Date" in the Mapped Field column at the line with value "CallDate"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    And I can see the file information:
       | DATA_SOURCE_NAME   | FILE_TYPE  |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |  NO_LOCATION_FILE  |   CSV      |      qa      |       0         |        0         |      0         |

Scenario: Importing new data to a file with no location
    Given I am already in the import page
    And I already have a file with "NO_LOCATION_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "No Location Event Data" value
    And I select the file with no location to be imported
    And I click in the button "Save"
    Then I can see the toast message "Import file has been submitted successfully"
    And I can see the informations:
      |  Name                    | Status   | User  | Imported_File_Name                                              | Total_Records   |
      |  No Location Event Data  | Ingested |  qa   | /var/metahub/upload/NO_LOCATION_FILE/mini-linkchart.csv.watched |      14         |

Scenario: Consulting imported information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/November/2020" until day "30"
    And I click in the search button
    Then I can see the total results "14"

Scenario: Deleting event data imported with a JSON file
    Given I am already in the import page
    And I already have a file with "NO_LOCATION_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "No Location Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a JSON Data Source file with success
    Given I am already in the import page
    And I already have a file with "NO_LOCATION_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"

Scenario: Verify deleted JSON information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/November/2020" until day "30"
    And I click in the search button
    Then I can see the total results "0"
