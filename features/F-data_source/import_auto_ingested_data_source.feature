@all
@datasource
@autoingestdatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can auto ingest event data to a data source

Background:
    Given I am logged in metahub

Scenario: Importing a new data source file
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "JSON_FILE"
    And I select the option "Regular" in the Data Source Type field
    And I select the event JSON file to be imported
    And I select the option "Unknown_Protocol" in the Event Type field
    And I select the option "Event Date" in the Mapped Field column at the line with value "Date"
    And I select the option "Latitude" in the Map as column at the line with value "Latitude"
    And I select the option "Longitude" in the Map as column at the line with value "Longitude"
    And I select the checkbox of the fields "Latitude" and "Longitude"
    And I click in the button "Merge Fields"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file information:
       | DATA_SOURCE_NAME   | FILE_TYPE  |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |     JSON_FILE      |   JSON     |      qa      |       0         |        0         |      0         |

Scenario: Auto ingesting data
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    When we copy a import file to the folder "/var/metahub/upload/JSON_FILE"
    And I click in the option "View"
    And I click in the refresh button until the import is loaded
    Then I can see the auto ingested informations:
      | Status   |     User     | Imported_File_Name                                       | Total_Records  |
      | Ingested | file-watcher | /var/metahub/upload/JSON_FILE/cdr_rand_vn_2.json.watched |      10000     |

Scenario: Consulting auto ingested JSON information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    Then I can see the total results "10,000"

Scenario: Deleting event data auto ingeste a JSON file
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a JSON Data Source file with success
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"
