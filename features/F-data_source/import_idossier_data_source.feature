#language: en

@all
@datasource
@importidossier
@smoketest
Feature: Import Idossier Data Source
    As a Metahub user
    Wants to manage data source information 
    So I can import idossier data

Background:
    Given I am logged in metahub

Scenario: Importing a new iDossier data source file with success
    Given I am already in the import page
    When I click in the "iDossier Data Sources" menu
    And I click in the "New Data Source" button
    And I fill the field Data Source Name with the value "iDossier"
    And I fill the field Data Source Description with the value "iDossier_file_test"
    And I select the iDossier csv file to be imported
    And I select the option "name" in the Mapped Field column at the line with value "Name"
    And I select the option "Free Text" in the Information Type column and "Basic Info" in the column Category at the line "Hobbies"
    And I select the option "Text" in the Data Type column and "Free Text" in the column Information Type and "Mobile Phone" in the column Category at the line "Mobile Phone"
    And I select the option "profileImage" in the Mapped Field column at the line with value "Profile Image"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    And I can see the idossier file informations:
       | DATA_SOURCE_NAME  | DESCRIPTION        |  CREATED_BY  |
       |   iDossier        | iDossier_file_test |      qa      |

Scenario: Importing new data to iDossier with image
    Given I am already in the import page
    When I click in the "iDossier Data Sources" menu
    And I click in the option "Import"
    And I fill the field Import Name with the value "iDossier_data"
    And I select the iDossier csv file to be imported
    And I select the image file to be imported
    And I click in the button "Save"
    Then I can see the toast message "iDossier import is successfull"
    And I click in the option "View"
    And I can see the importation results:
       |  IMPORTED_FILE_NAME                         |  IMPORTED_FILE_NAME_SECOND_LINE                       |
       |  /var/metahub/idossier/upload/image/images  |  /var/metahub/idossier/upload/csv/test_idossier.csv   |

Scenario: Consulting the data in the iDossier page
    Given I am already in the iDossier page
    And I can see the iDossier data:
       | NAME_LINE_1  | NAME_LINE_2   |  NAME_LINE_3  | NAME_LINE_4 | NAME_LINE_5 |
       |   Max2       |   Max3        |    Max4       |   Max5      |   Max6      |

Scenario: Deleting a Idossier Data Source file with success
    Given I am already in the import page
    And I already have a file with "iDossier" name imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "iDossier  successfully deleted."
    And I click in the refresh option
    And I can read the text information "No results!"
