#language: en

@all
@datasource
@eventdatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can import event data

Background:
    Given I am logged in metahub

Scenario: Importing a new data source CDR file using the BSA data source information with success
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "CDR_FILE"
    And I fill the field Data Source Description with the value "CDR_FILE_TEST"
    And I select the option "Regular" in the Data Source Type field
    And I select the event csv file to be imported
    And I select the option "CDR" in the Event Type field
    And I select the option "Text" in the column Data Type and "IMSI" in the column Mapped Field, both in the line with "IMSI" Field Name
    And I select the option "Text" in the column Data Type and "Sender" in the column Mapped Field, both in the line with "A_SUBS" Field Name
    And I select the option "Text" in the column Data Type and "IMEI" in the column Mapped Field, both in the line with "IMEI" Field Name
    And I select the option "DateTime" in the column Data Type and "Event Date" in the column Mapped Field, both in the line with "Datetime" Field Name
    And I select the option "Text" in the column Data Type and "Recipient" in the column Mapped Field, both in the line with "B_SUBS" Field Name
    And I select the option "MCC" in the column Map As in the line with "MCC" Field Name
    And I select the option "MNC" in the column Map As in the line with "MNC" Field Name
    And I select the option "LAC" in the column Map As in the line with "LAC" Field Name
    And I select the option "CellID" in the column Map As in the line with "CELLID" Field Name
    And I unselect the checkbox in line with value "CellID"
    And I select the checkbox of the fields "MCC", "MNC", "LAC" and "CellID"
    And I click in the button "Merge Fields"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file informations:
       | DATA_SOURCE_NAME  | DESCRIPTION   | FILE_TYPE |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |     CDR_FILE      | CDR_FILE_TEST |   CSV     |      qa      |       0         |        0         |      0         |     

Scenario: Importing new data to CDR Data Source
    Given I am already in the import page
    And I already have a file with "CDR_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "Event Data" value
    And I select the event csv file to be imported
    And I click in the button "Save"
    Then I can see the toast message "Import file has been submitted successfully"
    And I can see the informations:
      |  Name        | Status   | User  | Imported_File_Name                                                      | Total_Records |
      |  Event Data  | Ingested |  qa   | /var/metahub/upload/CDR_FILE/cdr_2022-06-15_18-40-47-603415.csv.watched |      150      |

Scenario: Consulting imported information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2022" and the end date "21/June/2022"
    And I click in the search button
    Then I can see the total results "150"

Scenario: Consulting imported information in analysis page with advanced query
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2022" and the end date "21/June/2022"
    And I click in the search button
    And I select the option Advanced Query
    And I fill the field Advanced Query with the value 'DURATION "Greater Than" 2000 '
    And I click in the advanced search button
    Then I can see the total results "124"

Scenario: Consulting imported information in analysis page with geolocation
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2022" and the end date "21/June/2022"
    And I click in the search button
    And I click on the option With Geolocation
    Then I can see the total Geo Locations Found: "150"

Scenario: Deleting event data of the CDR data source
    Given I am already in the import page
    And I already have a file with "CDR_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a CDR Data Source file with success
    Given I am already in the import page
    And I already have a file with "CDR_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"

Scenario: Verify deleted information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "14/January/2022" until day "21"
    And I click in the search button
    Then I can see the total results "0"
