@all
@datasource
@streamingdatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can import a json file by streaming proccess

Background:
    Given I am logged in metahub

Scenario: Importing a new data source json file by streaming process with success
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "STREAMING_DATA_SOURCE"
    And I select the option "Regular" in the Data Source Type field
    And I select the checkbox Allow streaming type
    And I select the streaming JSON file to be imported
    And I select the option "Unknown_Protocol" in the Event Type field
    And I select the option "DateTime" in the column Data Type and "Event Date" in the column Mapped Field, both in the line with "original_event.application_name" Field Name
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file information:
       | DATA_SOURCE_NAME       | FILE_TYPE  |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       | STREAMING_DATA_SOURCE  |   JSON     |      qa      |       0         |        0         |      0         |

Scenario: Importing new data to a streaming Data Source
    Given I am already in the import page
    And I already have a file with "STREAMING_DATA_SOURCE" imported
    When I send the import file by streaming api
    And I click in the option "View"
    Then I can see the stream file informations:
    |     Status         | User  |                  Imported_File_Name                                                         | Total_Records |
    | Streaming Ingested |  qa   | /var/metahub/upload/STREAMING_DATA_SOURCE/small-enriched-data-new-fields-2.json.csv.watched |      1        |

Scenario: Consulting  streaming imported information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2022" and the end date "30/June/2022"
    And I click in the search button
    Then I can see the total results "1"

Scenario: Deleting event data of the streaming data source
    Given I am already in the import page
    And I already have a file with "STREAMING_DATA_SOURCE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a streaming Data Source file with success
    Given I am already in the import page
    And I already have a file with "STREAMING_DATA_SOURCE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"

Scenario: Verify deleted streaming information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2022" and the end date "30/June/2022"
    And I click in the search button
    Then I can see the total results "0"
     