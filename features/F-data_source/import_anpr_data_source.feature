@all
@datasource
@anprdatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage non-celltower information 
    So I can import non-celltower data

Background:
    Given I am logged in metahub

Scenario: Importing a new data source ANPR file with success using BSA data
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "ANPR_FILE"
    And I fill the field Data Source Description with the value "ANPR_FILE_TEST"
    And I select the option "Regular" in the Data Source Type field
    And I select the option "ANPR" in the Event Type field
    And I select the ANPR csv file to be imported
    And I select the option "Event Date" in the Mapped Field column at the line with value "anprDate1"
    And I select the option "Ingest Date" in the Mapped Field column at the line with value "DateEntered1"
    And I select the option "Latitude" in the Map as column at the line with value "lat11"
    And I select the option "Longitude" in the Map as column at the line with value "lon11"
    And I select the option "Latitude" in the Map as column at the line with value "lat21"
    And I select the option "Longitude" in the Map as column at the line with value "lon21"
    And I select the option "Latitude" in the Map as column at the line with value "lat31"
    And I select the option "Longitude" in the Map as column at the line with value "lon31"
    And I unselect the checkbox in line with value "lon31"
    And I select the checkbox of the fields "lat11" and "lon11" and click in "Merge Fields"
    And I select the checkbox of the fields "lat21" and "lon21" and click in "Merge Fields"
    And I select the checkbox of the fields "lat31" and "lon31" and click in "Merge Fields"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file informations:
       | DATA_SOURCE_NAME  | DESCRIPTION    | FILE_TYPE |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |     ANPR_FILE     | ANPR_FILE_TEST |   CSV     |      qa      |       0         |        0         |      0         |

Scenario: Importing new data to ANPR Data Source
    Given I am already in the import page
    And I already have a file with "ANPR_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "Event Data" value
    And I select the ANPR csv file to be imported
    And I click in the button "Save"
    Then I can see the toast message "Import file has been submitted successfully"
    And I can see the informations:
      |  Name        | Status   | User  | Imported_File_Name                                     | Total_Records |
      |  Event Data  | Ingested |  qa   | /var/metahub/upload/ANPR_FILE/anpr_small_2.csv.watched |      10       |


Scenario: Deleting event data of the ANPR data source
    Given I am already in the import page
    And I already have a file with "ANPR_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a ANPR Data Source file with success
    Given I am already in the import page
    And I already have a file with "ANPR_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"
