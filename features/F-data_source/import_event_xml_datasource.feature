@all
@datasource
@xmldatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can import event data with a XMS file

Background:
    Given I am logged in metahub

Scenario: Importing a new data source event file with XMl format
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "XML_FILE"
    And I select the option "Regular" in the Data Source Type field
    And I select the option "ANPR" in the Event Type field
    And I select the event XML file to be imported
    And I select the option "Event Date" in the Mapped Field column at the line with value "anprDate"
    And I select the option "Ingest Date" in the Mapped Field column at the line with value "dateCreated"
    And I select the option "Latitude" in the Map as column at the line with value "lat1"
    And I select the option "Longitude" in the Map as column at the line with value "lon1"
    And I select the option "Latitude" in the Map as column at the line with value "lat2"
    And I select the option "Longitude" in the Map as column at the line with value "lon2"
    And I select the option "Latitude" in the Map as column at the line with value "lat3"
    And I select the option "Longitude" in the Map as column at the line with value "lon3"
    And I unselect the checkbox in line with value "lon3"
    And I select the checkbox of the fields "lat1" and "lon1" and click in "Merge Fields"
    And I select the checkbox of the fields "lat2" and "lon2" and click in "Merge Fields"
    And I select the checkbox of the fields "lat3" and "lon3" and click in "Merge Fields"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong1"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong2"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file information:
       | DATA_SOURCE_NAME   | FILE_TYPE  |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |     XML_FILE       |    XML     |      qa      |       0         |        0         |      0         |

Scenario: Importing new data to ANPR XML Data Source
    Given I am already in the import page
    And I already have a file with "XML_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "XML Event Data" value
    And I select the event XML file to be imported
    And I click in the button "Save"
    Then I can see the toast message "Import file has been submitted successfully"
    And I can see the informations:
      |  Name             | Status   | User  | Imported_File_Name                                 | Total_Records |
      |  XML Event Data   | Ingested |  qa   | /var/metahub/upload/XML_FILE/anpr_test.xml.watched |       9       | 

Scenario: Consulting imported XML information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    Then I can see the total results "9"

Scenario: Consulting imported JSON information in analysis page with advanced query
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    And I select the option Advanced Query
    And I fill the field Advanced Query with the value 'model "Does Not Contain" Corolla'
    And I click in the advanced search button
    Then I can see the total results "6"

Scenario: Deleting event data imported with a XML file
    Given I am already in the import page
    And I already have a file with "XML_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "XML Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a XML Data Source file with success
    Given I am already in the import page
    And I already have a file with "XML_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"

Scenario: Verify deleted XMS information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    Then I can see the total results "0"
 