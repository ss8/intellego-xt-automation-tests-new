@all
@datasource
@jsondatasource
@smoketest
Feature: Event Data Source Managment
    As a Metahub user
    Wants to manage event information 
    So I can import event data with a json file

Background:
    Given I am logged in metahub

Scenario: Importing a new data source event file with JSON format
    Given I am already in the import page
    When I click in the New Data Source button
    And I fill the field Data Source Name with the value "JSON_FILE"
    And I select the option "Regular" in the Data Source Type field
    And I select the event JSON file to be imported
    And I select the option "Unknown_Protocol" in the Event Type field
    And I select the option "Event Date" in the Mapped Field column at the line with value "Date"
    And I select the option "Latitude" in the Map as column at the line with value "Latitude"
    And I select the option "Longitude" in the Map as column at the line with value "Longitude"
    And I select the checkbox of the fields "Latitude" and "Longitude"
    And I click in the button "Merge Fields"
    And I select the option "Event Locations" in the Mapped Field column at the line with value "Merged_LatLong"
    And I click in the button "Save"
    Then I can see the toast message "Data Source has been created successfully"
    Then I can see the file information:
       | DATA_SOURCE_NAME   | FILE_TYPE  |  CREATED_BY  |  TOTAL_RECORDS  |  FAILED_RECORDS  | TOTAL_IMPORTS  |
       |     JSON_FILE      |   JSON     |      qa      |       0         |        0         |      0         |     

Scenario: Importing new data with a JSON file
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    And I click in the option "Import"
    And I fill the field Import Name with the "JSON Event Data" value
    And I select the event JSON file to be imported
    And I click in the button "Save"
    Then I can see the toast message "Import file has been submitted successfully"
    And I can see the informations:
      |  Name             | Status   | User  | Imported_File_Name                                       | Total_Records   |
      |  JSON Event Data  | Ingested |  qa   | /var/metahub/upload/JSON_FILE/cdr_rand_vn_2.json.watched |      10000      | 

Scenario: Consulting imported JSON information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    Then I can see the total results "10,000"

Scenario: Consulting imported JSON information in analysis page with advanced query
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "1/January/2011" and the end date "3/March/2022"
    And I click in the search button
    And I select the option Advanced Query
    And I fill the field Advanced Query with the value '"Target End Cell Description" Equals 2G_HIDDNVALLEYMGC'
    And I click in the advanced search button
    Then I can see the total results "228"


Scenario: Deleting event data imported with a JSON file
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    When I click in the option "View"
    And I click in the trash button and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "JSON Event Data successfully deleted."
    And I click in the refresh button
    And I can see the text information "There are no imports yet"

Scenario: Deleting a JSON Data Source file with success
    Given I am already in the import page
    And I already have a file with "JSON_FILE" imported
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "Data Source has been deleted successfully"
    And I click in the refresh button
    And I can see the text information "No results!"

Scenario: Verify deleted JSON information in analysis page
    Given I am already in the analysis page
    When I click on the calendar
    And I select the initial date "25/January/2022" until day "26"
    And I click in the search button
    Then I can see the total results "0"
 