#language: en

@all
@login
Feature: Verify the Login Screen
    As a Metahub user
    Wants to login to Metahub with username and password
    So I can get access to all the existing resources
@smoketest
Scenario: Realize login and logout to Metahub with success
    Given I am already in the Metahub login page
    And I fill the fields username and password
    When I click submit
    Then I can see the Metahub Home Page
    When I click menu button
    And I click logout
    Then I logout of the metahub

Scenario Outline: Wrong password or username
    Given I am already in the Metahub login page
    And I fill the field Username with "<USERNAME>" and Password with "<PASSWORD>"
    When I click submit
    Then I can see the message "<MESSAGE>"
Examples:
       | USERNAME  | PASSWORD  | MESSAGE                                |
       | grpadm    | 1234      | Wrong password or username. Try again. |
       | abcd      | 1234      | Wrong password or username. Try again. |

Scenario: Realize login and logout to Metahub with adm user with success
    Given I am already in the Metahub login page
    And I fill the fields with adm username and password 
    When I click submit
    Then I can see the Adm Metahub Home Page
    When I click menu button
    And I click logout
    Then I logout of the metahub
