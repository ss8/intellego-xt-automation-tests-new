#language: en

@all
@idossier
@smoketest
Feature: Idossier managment
    As a Metahub user
    Wants to manage Idossier information
    So I can have idossier data to consult

Background:
    Given I am logged in metahub

Scenario: Creating new iDossier data with success
    Given I am already in the iDossier page
    And there is no Idossier information on the system
    When I click in the "New iDossier" button
    And I fill the field Enter iDossier Name with the value "iDossier name"
    And I fill the field Enter Description with the value "iDossier description"
    And I select a profile image to upload
    And I click in the "Upload" button
    And I click in the "Add category" button
    And I fill the field category with the value "Basic Info"
    And I click in the "Add category" button
    And I click in the "Add a card..." button
    And I click in the "Select the information type" button
    And I select the option "Free Text"
    And I click in the "Select Field" button
    And I select the option "Description"
    And I fill the field Description with the value "Category description"
    And I fill the text area Description with the value "Category text area description"
    And I click in the "Add" button
    And I click in the "Add category" button
    And I fill the field category with the value "Mobile Phone"
    And I click in the "Add category" button
    And I click in the "Add a card..." button
    And I click in the "Select the information type" button
    And I select the option "Free Text"
    And I click in the "Select Field" button
    And I select the option "Description"
    And I fill the field Description with the value "Phone number"
    And I fill the text area Description with the value "(99) 99999-9999"
    And I click in the "Add" button
    And I click in the button "Save"
    Then I can see the toast message "iDossier updated successfully"
    And I can see the iDossier informations: 
       |      NAME         | DESCRIPTION          |  CREATED_BY  | UPDATED_BY | CATEGORIES | CARDS |
       |   iDossier name   | iDossier description |      qa      |      qa    |      2     |   2   |

Scenario: Editing iDossier information using the edit option
    Given I am already in the iDossier page
    And I already have the iDossier with "iDossier name" name in the system
    When I click in the option "Edit"
    And I edit the infomation of the field iDossier name to " Edited"
    And I edit the infomation of the field iDossier description to " Edited"
    And I click in the button "Save"
    And I can see the iDossier informations:
       |      NAME               | DESCRIPTION                 |  CREATED_BY  | UPDATED_BY | CATEGORIES | CARDS |
       |  iDossier name Edited   | iDossier description Edited |      qa      |      qa    |      2     |   2   |

Scenario: Editing iDossier information using the view option plus the edit button
    Given I am already in the iDossier page
    And I already have the iDossier with "iDossier name Edited" name in the system
    When I click in the option "View"
    And I click in the button "Edit"
    And I click in the first card
    And I edit the informations
    And I click in the button "Ok"
    And I click in the button "Save"
    Then I can see the field Description with value "Category description Edited"
    And I can see the text area description with the value "Category text area description Edited"

Scenario: Deleting iDossier data with success

    Given I am already in the iDossier page
    And I already have the iDossier with "iDossier name Edited" name in the system
    When I click in the option "Delete" and confirm pressing the option "Yes, delete it"
    Then I can see the toast message "iDossier deleted Successfully"
    And I cannot see the iDossier with "iDossier name Edited" name in the system
