@all
@analysis
@alternativeAnalysis
Feature: Alternative Analysis Managment
    As an analyst user,
    I want to search some information on Analysis tab
    I will analyze and process the information.

Background:
    Given I am logged in metahub
    And I am already in the Metahub analysis page

Scenario: Sample query without end date
    Given The calendar is already open
    When I select only initial date "1/January/2011"
    Then I can see the search button is disabled

Scenario: Sample query without start date
    Given The calendar is already open
    When I select only end date "2/January/2011"
    Then I can see the search button is disabled
