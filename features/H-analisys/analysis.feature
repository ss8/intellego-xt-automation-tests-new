@all
@analysis
Feature: Analysis Managment
    As an analyst user,
    I want to search some information on Analysis tab
    I will analyze and process the information.

Background:
    Given I am logged in metahub
    And I am already in the Metahub analysis page

Scenario Outline: Valid sample query with date filters
    Given I already added data source by api
    And I already added import to data source by api
    When I click on the calendar
    And I click in the date button "<BUTTON>"
    And I click in the search button
    Then I delete import of data source by api
    And I wait import are deleted
    And I delete data source by api
    And Analysis results are equal "<RESULT>"
Examples:
       | BUTTON        | RESULT |
       | Today         | 1,000  |
       | Yesterday     | 1,000  |
       | Past week     | 2,000  |
       | Past month    | 3,000  |
       | Past 3 months | 4,000  |
       | Past 6 months | 5,000  |
       | Past year     | 6,000  |
       | Past 2 years  | 9,000  |
