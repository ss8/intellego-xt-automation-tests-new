const dotenv = require('dotenv');
const dotenvParseVariables = require('dotenv-parse-variables');

let env = dotenv.config({})
if (env.error) throw env.error;
env = dotenvParseVariables(env.parsed);

exports.config = {
	output: './output',
	helpers: {
		WebDriver: {
			url: env.BASE_URL,
			browser: env.BROWSER,
			host: '127.0.0.1',
			port: 4444,
			restart: false,
			windowSize: env.WINDOW_SIZE,
			desiredCapabilities: {
				chromeOptions: {
					args: [ /*"--headless",*/ "--disable-gpu", "--no-sandbox"]
				}
			}
		},
		FileSystem: {},
		CustomHelper: {
			require: './support/helpers/custom_helper.js',
		},
		ResembleHelper: {
			require: 'codeceptjs-resemblehelper',
			screenshotFolder: './output',
			baseFolder: './support/screenshots/base/',
			diffFolder: './support/screenshots/diff/'
		}
	},
	include: {
		I: './steps_file.js',
		LoginPage: './page_objects/login_page.js',
		HomePage: './page_objects/home_page.js',
		DataSourcePage: './page_objects/dataSource_page.js',
	},
	mocha: {},
	bootstrap: null,
	teardown: null,
	hooks: [],
	gherkin: {
		features: './features/*/*.feature'
	},
	plugins: {
		allure: {
			enabled: true,
			outputDir: "./output/allure/allure-results"
		},
		screenshotOnFail: {
			enabled: true
		},
		skipper: {
			require: './support/plugins/skipper.js',
			enabled: true
		},
		wdio: {
			enabled: true,
			services: ['selenium-standalone']
		}
	},
	tests: './step_definitions/*/*_test.js',
	name: 'e2e'
}
