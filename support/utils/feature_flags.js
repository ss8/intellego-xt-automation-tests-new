class Singleton {

  getInstance() {
    return Singleton.instance;
  }
}

module.exports = Singleton;
