const { I } = inject();
const BasePage = require('./base_page.js');


class LoginPage extends BasePage {
	constructor() {
		super();
		this.pageTitle = "//*[@class='form']";
		this.userInput = 'input[name="login"]';
		this.passwordInput = 'input[name="password"]';
		this.loginButton = 'button[type="submit"]';
		this.errorMessageLogin = "//*[@class='errorDisplay']";
	}

	async accessLoginPage() {
		await I.amOnPage('');
		await super.isCurrentPage();
	}

	async fillUserAndPass(user, pass) {
		await super.fillField(this.userInput, user);
		await super.fillField(this.passwordInput, pass);
	}

	async clickLogin() {
		await super.click(this.loginButton);
		await super.waitForLoaderInvisible();
	}
}

module.exports = new LoginPage();
module.exports.LoginPage = LoginPage;
