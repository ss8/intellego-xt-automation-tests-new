const { I } = inject();
const FormData = require('form-data');
const fs = require('fs');
const path = require('path');
const assert = require('assert');

class StreamingApi {

    async postStreamingFileApi() {
        let form = new FormData();
        form.append('file', fs.createReadStream(path.join(__dirname, './support/files/small-enriched-data-new-fields-2.json')));
        const url = (process.env.STREAM_URL + 'v1/streaming/Stream_STREAMING_DATA_SOURCE/consume?format=JSONARRAY');
        let res = await I.sendPostRequest(url, { ...form.getHeaders() });
        assert.equal(res.status, 200)
    };

    async postStreamingBodyUrl() {
        const postEndpointBodyFileUrl = (process.env.STREAM_URL + 'v1/streaming/Stream_STREAMING_DATA_SOURCE/consume');
        const bodyFilePostResponse = await I.sendPostRequest(postEndpointBodyFileUrl,
            [{
                "original_event": {
                    "application_name": "2022-07-03T20:00:00Z",
                    "application_category_name": "WebSite"
                },
                "cps_metadata": {
                    "host_details": {
                        "category": "local",
                        "context": "context",
                        "hostname": "127.0.0.1"
                    },
                    "user_profile_summary_service": "summary service",
                    "protocol_business_keys": "protocol business keys",
                    "free_text_summary": "testing summary..."
                },
                "network": { "type": "http" },
                "event": { "type": "Web" }
            }],
            {
                'Content-Type': 'application/json',
            },
        )
        return bodyFilePostResponse;
    };
};

module.exports = new StreamingApi();
module.exports.StreamingApi = StreamingApi;
