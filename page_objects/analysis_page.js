const { I } = inject();
const BasePage = require('./base_page.js');


class AnalysisPage extends BasePage {
	constructor() {
		super();
        this.analysisMenu = '#analysis';
		this.buttonDiagramTree = 'span[icon="diagram-tree"]';
		this.buttonCalendar = 'span[icon="calendar"]';
		this.selectMonthButton = '//div[contains(@class,"bp3-html-select bp3-minimal bp3-datepicker-month-select")]';
		this.selectMonthOption = '//div[contains(@class,"bp3-html-select bp3-minimal bp3-datepicker-month-select")]/select';
		this.selectYearButton = '//div[contains(@class,"bp3-html-select bp3-minimal bp3-datepicker-year-select")]';
		this.selectYearOption = '//div[contains(@class,"bp3-html-select bp3-minimal bp3-datepicker-year-select")]/select';
		this.selectEndMonthButton = '(//select)[3]';
		this.selectEndYearButton = '(//select)[4]';
		this.selectDay = "//div[@class='DayPicker-Day'][text()='<VALUE>']";
		this.unselectDay ="//div[@aria-selected='true'][text()='<VALUE>']";
		this.analysisSearchButton = 'span[icon="search-text"]';
		this.disabledSearchButton = '//*[@class="bp3-button bp3-disabled bp3-intent-primary"]';
		this.resultsFilter = '//*[@class="result-container"]//span[@class="info"]';
		this.resultsFilterGeoLocation = '//*[@class="map"]//span[@class="info"]';
		this.advancedQueryButton = "//div[@class='bp3-switch-inner-text'][text()='Simple Query']";
		this.imputAdvancedQueryField = "//div[@class='queryEditor']";
		this.analysisAdvancedSearchButton = "//button[@type='submit']";
		this.withGeolocationButton = "//label[@class='bp3-control bp3-switch bp3-inline geoSwitch']";
		
	}	
}

module.exports = new AnalysisPage();
module.exports.AnalysisPage = AnalysisPage;
