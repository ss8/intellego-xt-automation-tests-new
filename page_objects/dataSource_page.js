const { I } = inject();
const BasePage = require('./base_page.js');


class DataSourcePage extends BasePage {
	constructor() {
		super();
		this.importMenu = '#import';
		this.newDataSourceButton = 'button[class="bp3-button"]';
		this.dataSourceNameImput = '#data-source-input-name';
		this.dataSourceDescription = 'input[name="desc"]';
		this.dataSourceTypeDropDown = 'select[name="datasourceType"]';
		this.eventTypeDropDown = "//span[contains(text(),'Select Type')]";
		this.eventTypeInputSearch = 'button[class="bp3-input"]';
		this.SelectOptionField = '//div[contains(@class,"bp3-text-overflow-ellipsis bp3-fill") and text()="<VALUE>"]';
		this.uploadDataSourceFile = '#uploadFile';
		this.tableColumn = '//div[contains(@class,"ag-cell ag-cell-not-inline-editing ag-cell-auto-height ag-cell-value") and text()="<VALUE>"]';
		this.fieldValue = "//div[@role='option'][text()='<VALUE>']";
		this.mapAsField = "(//div[@tabindex='-1'][text()='<VALUE>'])";
		this.buttons = "//span[@class='bp3-button-text'][text()='<VALUE>']";
		this.fieldTableDataSource = "//div[@tabindex='-1'][text()='<VALUE>']"
		this.toastMessage = "//span[@class='bp3-toast-message'][text()='<VALUE>']";
		this.threeDots = 'span[icon="more"]';
		this.importImputNameDataSource = '#import-input-name';
		this.uploadImageZipFile = '//label[@id="imageZipFile"]/input';
		this.importNameDataSource = "input[name='data-source-import-name']";
		this.buttonTrash = 'span[icon="trash"]';
		this.buttonRefresh = 'span[icon="refresh"]';
		this.screenTableMessage = "//h4[@class='bp3-heading'][text()='<VALUE>']";
		this.tableDataSourceMappedField = "//div[@tabindex='-1'][text()='<VALUE>']/../div[@col-id='mappedField']";
		this.tableDataSourceMapAsdField = "//div[@tabindex='-1'][text()='<VALUE>']/../div[@col-id='taggedField']";
		this.tableDataSourceDataTypedField = "//div[@tabindex='-1'][text()='<VALUE>']/../div[@col-id='dataType']";
		this.tableDataSourceCategoryField = "//div[@tabindex='-1'][text()='<VALUE>']/../div[@col-id='category']";
		this.tableDataSourceFieldType = "//div[@tabindex='-1'][text()='<VALUE>']/../div[@col-id='fieldType']";
		this.tableDataSourceCheckbox = "//div[@tabindex='-1'][text()='<VALUE>']/..//input[@class='ag-input-field-input ag-checkbox-input']";
		this.tableStatusValue = '//*[@id="bp3-tab-panel_tabsRoot_firstTab"]//*[@col-id="status"][@role="gridcell"]';
		this.allowStreamingCheckBox = "//label[@class='bp3-control bp3-checkbox switch-choice'][text()='Allow streaming type']";
	}	
}

module.exports = new DataSourcePage();
module.exports.DataSourcePage = DataSourcePage;
