const BasePage = require('./base_page.js');
const { I } = inject();
const timeout = 30;

class HomePage extends BasePage {
	constructor() {
		super();
		this.peopleMenu = '#people';
		this.analysisMenu = '#analysis';
		this.monitoringMenu = '#monitoring';
		this.dashboardMenu = '#dashboard';
		this.idossierMenu = '#idossier';
		this.importMenu = '#import';
		this.menuButton = '//*[@class="bp3-button bp3-minimal preferences-menu-button"]';
		this.textOverflow = '//*[@class="bp3-text-overflow-ellipsis bp3-fill"][text()="<VALUE>"]'
	}

	async asyncisCurrentPage() {
		await super.isCurrentPage();
		await super.waitForLoaderInvisible();
	}

	async isHomePage() {
		I.waitForElement('#people', timeout);
		I.waitForElement('#analysis', timeout);
		I.waitForElement('#monitoring', timeout);
		I.waitForElement('#dashboard', timeout);
		I.waitForElement('#idossier', timeout);
		I.waitForElement('#import', timeout);
	}
}

module.exports = new HomePage();
module.exports.HomePage = HomePage;
