const BasePage = require('./base_page.js');
const { I } = inject();
const timeout = 30;

class AdmHomePage extends BasePage {
    constructor() {
        super();
        this.peopleMenu = '#people';
        this.usersMenu = '#users';
        this.teamsMenu = '#teams';
        this.Menu = '#roles';

    }

    async isAdmHomePage() {
        I.waitForElement('#people', timeout);
        I.waitForElement('#users', timeout);
        I.waitForElement('#teams', timeout);
        I.waitForElement('#roles', timeout);
    }
}

module.exports = new AdmHomePage();
module.exports.AdmHomePage = AdmHomePage;
