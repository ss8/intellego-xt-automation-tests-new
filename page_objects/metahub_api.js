const { I } = inject();
const ls = require('local-storage');
const FormData = require('form-data');
const fs = require('fs');
const path = require('path');
const assert = require('assert');

class MetahubApi {

    async getSessionToken() {
        {
            const postEndpointUrlAuth = (process.env.BASE_URL + 'metahub/api/v1/users/authenticate');
            const authPostResponse = await I.sendPostRequest(postEndpointUrlAuth,
                { "user": process.env.METAHUB_ADM_USER, "password": process.env.METAHUB_ADM_PASS });
            ls('x-auth-token', authPostResponse.headers['x-auth-token']);
            return authPostResponse;
        }
    }

    async getSessionTokenWithQAUser() {
        {
            const postEndpointUrlAuth = (process.env.BASE_URL + 'metahub/api/v1/users/authenticate');
            const authPostResponse = await I.sendPostRequest(postEndpointUrlAuth,
                { "user": process.env.METAHUB_USER, "password": process.env.METAHUB_PASS });
            ls('x-auth-token', authPostResponse.headers['x-auth-token']);
            return authPostResponse;
        }
    }

    async deleteUser(user) {
        const deleteEndpointUrlUsers = (process.env.BASE_URL + 'metahub/api/v1/users/' + user);
        const deleteUserResponse = await I.sendDeleteRequest(deleteEndpointUrlUsers,
            {

                'x-auth-token': ls.get('x-auth-token'),
            },
        );
        return deleteUserResponse;
    };

    async createNewUser(userID) {
        const postEndpointUrlUsers = (process.env.BASE_URL + 'metahub/api/v1/users');
        const userPostResponse = await I.sendPostRequest(postEndpointUrlUsers,
            {
                "userID": userID,
                "password": "Test123456!",
                "role": "",
                "firstName": "",
                "middleName": "",
                "lastName": "",
                "displayName": "qa",
                "office": "",
                "dept": "",
                "email": "",
                "phone": "",
                "state": "active",
                "superior": 0,
                "validLicenses": [
                ],
                "privileges": [
                ],
                "reporting": {
                    "assignedUsers": [
                        {
                            "id": "dolor cup",
                            "name": "qui sit commodo mollit"
                        },
                        {
                            "id": "officia mollit reprehenderit ut",
                            "name": "nostrud commodo veniam eu"
                        }
                    ]
                },
                "intellegoUser": false
            },
            {
                'x-auth-token': ls.get('x-auth-token'),
            },

        )
        return userPostResponse;
    };

    async updateUserRole(userID) {
        const postEndpointUrlRole = (process.env.BASE_URL + 'metahub/api/v1/users/' + userID + '/roles');
        const rolePostResponse = await I.sendPostRequest(postEndpointUrlRole,
            ["Manage Users",
                "Manage Teams",
                "Manage Roles",
                "Manage Alarms",
                "Manage Audit Log",
                "Manage Configuration",
                "Manage Dashboards/Reports",
                "Manage DataSource and Data Imports",
                "Manage Query Permissions",
                "Manage Idossier Permissions",
                "Manage Notifications",
                "Manage Area Permissions"],
            {
                'x-auth-token': ls.get('x-auth-token'),
            },
        )
        return rolePostResponse;
    };
    
    async getRoles(){
        const getEndPointUrlRole = (process.env.BASE_URL+'metahub/api/v1/roles');
        const roleGetResponse= await I.sendGetRequest(getEndPointUrlRole,{
            'x-auth-token': ls.get('x-auth-token'),
        });
        return roleGetResponse;
    }
    
    async deleteRole(roleId){
        const deleteEndPointUrlRole = (process.env.BASE_URL+'metahub/api/v1/roles/'+roleId);
        const roleDeleteResponse= await I.sendDeleteRequest(deleteEndPointUrlRole,{
            'x-auth-token': ls.get('x-auth-token'),
        });
        return roleDeleteResponse;
    }

    async createNewDataSource() {
        const postEndpointUrlUsers = (process.env.BASE_URL + 'metahub/api/v1/ingestdatasource');
        const dataSourcePostResponse = I.sendPostRequest(postEndpointUrlUsers,
            {
                "name": "JSON_FILE",
                "desc": "",
                "eventType": "Unknown_Protocol",
                "fileType": "JSON",
                "streamType": "",
                "id": 43,
                "userName": "test2",
                "createdDate": "2022-08-03T12:08:39Z",
                "importColumnsToTypeMap": {"Date":{"fieldName":"Date","dataType":"DateTime","mappedField":"eventDate","dateTimeFormat":null,"displayName":"Date","fieldsToMerge":null,"derivedField":"","mandatory":false},"Call Duration":{"fieldName":"Call Duration","dataType":"Text","mappedField":"callduration_txt","dateTimeFormat":null,"displayName":"Call Duration","fieldsToMerge":null,"derivedField":null,"mandatory":false},"From":{"fieldName":"From","dataType":"Number","mappedField":"from_db","dateTimeFormat":null,"displayName":"From","fieldsToMerge":null,"derivedField":null,"mandatory":false},"To":{"fieldName":"To","dataType":"Number","mappedField":"to_db","dateTimeFormat":null,"displayName":"To","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Case":{"fieldName":"Case","dataType":"Text","mappedField":"case_txt","dateTimeFormat":null,"displayName":"Case","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Subcase":{"fieldName":"Subcase","dataType":"Text","mappedField":"subcase_txt","dateTimeFormat":null,"displayName":"Subcase","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Target":{"fieldName":"Target","dataType":"Number","mappedField":"target_db","dateTimeFormat":null,"displayName":"Target","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Product ID":{"fieldName":"Product ID","dataType":"Number","mappedField":"productid_db","dateTimeFormat":null,"displayName":"Product ID","fieldsToMerge":null,"derivedField":null,"mandatory":false},"MSIISDN":{"fieldName":"MSIISDN","dataType":"Number","mappedField":"msiisdn_db","dateTimeFormat":null,"displayName":"MSIISDN","fieldsToMerge":null,"derivedField":null,"mandatory":false},"IMEI / ESN":{"fieldName":"IMEI / ESN","dataType":"Number","mappedField":"imeiesn_db","dateTimeFormat":null,"displayName":"IMEI / ESN","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Target Start Cell Id":{"fieldName":"Target Start Cell Id","dataType":"Text","mappedField":"targetstartcellid_txt","dateTimeFormat":null,"displayName":"Target Start Cell Id","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Target Start Cell Description":{"fieldName":"Target Start Cell Description","dataType":"Text","mappedField":"targetstartcelldescription_txt","dateTimeFormat":null,"displayName":"Target Start Cell Description","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Target End Cell Id":{"fieldName":"Target End Cell Id","dataType":"Text","mappedField":"targetendcellid_txt","dateTimeFormat":null,"displayName":"Target End Cell Id","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Target End Cell Description":{"fieldName":"Target End Cell Description","dataType":"Text","mappedField":"targetendcelldescription_txt","dateTimeFormat":null,"displayName":"Target End Cell Description","fieldsToMerge":null,"derivedField":null,"mandatory":false},"Merged_LatLong":{"fieldName":"Merged_LatLong","dataType":"GeoPoint","mappedField":"eventLocations","dateTimeFormat":null,"displayName":"LatitudeLongitude","fieldsToMerge":["Latitude","Longitude"],"derivedField":null,"mandatory":false}},
                "canDelete": true,
                "usStyleDateFormat": true,
                "importCount": 0,
                "datasourceFolderName": "/var/metahub/upload/JSON_FILE",
                "totalRecords": 0,
                "rowcount": 0,
                "failedrowcount": 0,
                "datasourceType": "Regular"
            },
            {
                'x-auth-token': ls.get('x-auth-token'),
            },
        )
        return dataSourcePostResponse;
    };

    async getDataSources(){
        const getEndPointUrlRole = (process.env.BASE_URL+'metahub/api/v1/ingestdatasource');
        const dataSourceGetResponse= I.sendGetRequest(getEndPointUrlRole,{
            'x-auth-token': ls.get('x-auth-token'),
        });
        return dataSourceGetResponse;
    }

    async getAllImportDataSources(){
        const postEndPointUrlRole =  (process.env.BASE_URL+'metahub/api/v1/ingestdatasource/dataimport/getAll');
        const res = I.sendPostRequest(postEndPointUrlRole,{
            "startRow": 0,
            "endRow": 1          
          },
          {
            'x-auth-token': ls.get('x-auth-token'),
        })
        return res
    }

    async postImportFile(dataSourceId,jsonFile) {
        I.setRequestTimeout(20000);
        const form = new FormData();
        form.append('name', 'json');
        form.append('datasource', dataSourceId);
        form.append('file',fs.createReadStream(jsonFile))
        const url = (process.env.BASE_URL + 'metahub/api/v1/ingestdatasource/dataimport');
        const res = await I.sendPostRequest(url, form, { 'x-auth-token': ls.get('x-auth-token'), ...form.getHeaders() });
        return res;
    };

    async deleteImportFile(importId){
        const deleteEndPointUrlRole = (process.env.BASE_URL+'metahub/api/v1/ingestdatasource/dataimport/'+importId);
        const importDeleteResponse= await I.sendDeleteRequest(deleteEndPointUrlRole,{
            'x-auth-token': ls.get('x-auth-token'),
        });
        return importDeleteResponse;
    }

    async deleteDataSourceFile(dataSourceId){
        const deleteEndPointUrlRole = (process.env.BASE_URL+'metahub/api/v1/ingestdatasource/'+dataSourceId);
        const dataSourceDeleteResponse= await I.sendDeleteRequest(deleteEndPointUrlRole,{
            'x-auth-token': ls.get('x-auth-token'),
        });
        return dataSourceDeleteResponse;
    }
    
    async generateFileWithActualDate(){
        const name = __dirname+'/../support/files/cdr_rand_vn_2.json';
        const oldFile = JSON.parse(fs.readFileSync(name).toString());

        const minute = 1000 * 60;
        const hour = minute * 60;
        const day = hour * 24;
        const week = day * 8;
        const month = day * 30;
        const year = day * 365;

        let newFile = "";
        for(let index=0; index<oldFile.length;index++){
            let currentObject = oldFile[index];
            if(index<1000){
              currentObject.Date = new Date(Date.now());
            }
            else if(index<2000){
              currentObject.Date = new Date(Date.now()-day);
            }
            else if(index<3000){
                currentObject.Date = new Date(Date.now()-day*3);
            }
            else if(index<4000){
                currentObject.Date = new Date(Date.now()-week*2);
            }
            else if(index<5000){
                currentObject.Date = new Date(Date.now()-month*2);
            }
            else if(index<6000){
                currentObject.Date = new Date(Date.now()-month*4);
            }
            else if(index<7000){
                currentObject.Date = new Date(Date.now()-month*8);
            }
            else{
                currentObject.Date = new Date(Date.now()-year-month);
            }

            if(index+1==oldFile.length){
              newFile=newFile+JSON.stringify(currentObject);
            }
            else{
              newFile=newFile+JSON.stringify(currentObject)+",\n"
            }
        }
        newFile="[\n"+newFile+"\n]";
        
        const jsonFile = __dirname+"/../support/files/cdr_rand_vn_2_atual_date.json";
        fs.writeFile(jsonFile, newFile,(err) => {
            if (err)
              console.log(err);
            }
          )
        return jsonFile
    }
};


module.exports = new MetahubApi();
module.exports.MetahubApi = MetahubApi;
