const { I } = inject();
const BasePage = require('./base_page.js');


class IdossierPage extends BasePage {
	constructor() {
		super();
		this.pageTitle = "//div[@class='idossier-import-container']";
		this.idossierMenuButton = "#idossier";
		this.idossierPageTable = "//span[@class='ag-cell-value'][text()='<VALUE>']";
		this.idossierInputNameField = "//span[@class='bp3-editable-text-content'][text()='Enter iDossier Name (required)']";
		this.idossierInputDescField = "//div[@class='bp3-editable-text bp3-editable-text-placeholder idossier-fields description-field ']";
		this.uploadImageButton = "(//IMG[@class='profile-image'])[2]";
		this.uploadFile = 'input[type="file"]';
		this.inputCategory = 'input[type="text"]';
		this.textAreaDescription = "//textarea[@name='description']";
		this.idossierEditName = "//div[@class='bp3-editable-text bp3-editable-text-editing idossier-fields name-field']";
		this.idossierEditDescription = "//span[@class='bp3-editable-text-content'][text()='iDossier description']";
		this.idossierEditCardDescription = '#Description';
		this.idossierCardClick = "//article[@class='sc-iBzEeX sc-cTJkRt glZmJR lmGFyU']";
	}

	isCurrentPage() {
		super.isCurrentPage();
		super.waitForLoaderInvisible();
	}
}

module.exports = new IdossierPage();
module.exports.IdossierPage = IdossierPage;
