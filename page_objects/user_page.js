const { I } = inject();
const BasePage = require('./base_page.js');


class UserPage extends BasePage {
	constructor() {
		super();
        this.pageTitle = "#users";
        this.inputNameUser = 'input[name="userID"]';
        this.inputEmailUser = 'input[name="email"]';
        this.inputFirstName = 'input[name="firstName"]';
        this.inputLastName = 'input[name="lastName"]';
        this.SelectOptionField = '//div[contains(@class,"bp3-text-overflow-ellipsis bp3-fill") and text()="<VALUE>"]';
        this.inputRoleField = '//*[@class ="bp3-input-ghost bp3-multi-select-tag-input-input"]';
        this.inputPasswordField = 'input[name="password"]';
        this.inputNewPasswordField = 'input[name="newpassword"]';
        this.inputConfirmNewPasswordField = 'input[name="confirmpassword"]';
        this.submitButton = "//button[@type='submit'][text()='submit']";
        this.inputUserSearchField = "#ag-27-input";
        this.userProfileInformation = "//div[@class='preferences-menu-username']";
        this.rolesMenu = "#roles";
        this.inputRoleName = 'input[name="name"]';
        this.textAreaDescription = "//textarea[@name='description']";
        this.checkBoxRolePermission = "//span[@class='rct-title'][text()='<VALUE>']";
        this.inputSearchRole = "//input[@type='text']";
        this.teamsMenu = "#teams";
        this.errorMessage = "//*[@class='bp3-toast-message']";
        this.fieldErrorMessage = "//*[@class='bp3-label field-error-message']";
	}

}

module.exports = new UserPage();
module.exports.UserPage = UserPage;