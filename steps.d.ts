/// <reference types='codeceptjs' />
type steps_file = typeof import('./steps_file.js');
type LoginPage = typeof import('./page_objects/login_page.js');
type HomePage = typeof import('./page_objects/home_page.js');
type DataSourcePage = typeof import('./page_objects/dataSource_page.js');
type CustomHelper = import('./support/helpers/custom_helper.js');
type ResembleHelper = import('codeceptjs-resemblehelper');

declare namespace CodeceptJS {
  interface SupportObject { I: I, current: any, LoginPage: LoginPage, HomePage: HomePage, DataSourcePage: DataSourcePage }
  interface Methods extends Puppeteer, WebDriver, FileSystem, CustomHelper, ResembleHelper {}
  interface I extends ReturnType<steps_file>, WithTranslation<FileSystem>, WithTranslation<CustomHelper>, WithTranslation<ResembleHelper> {}
  namespace Translation {
    interface Actions {}
  }
}
