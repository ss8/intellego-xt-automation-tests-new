# CodeceptJS - Metahub
A Metahub testing framework coded in CodeceptJS and Puppeteer.

## Setup

To run these tests, you will need the following:
* Any OS type
* Node.js 14.18.1
* Chrome 97 or later

```bash
$ npm install

## Get Metahub URL and configure .env file

Copy the `.env-template` file to a new file, and name it `.env`. In the `.env` file, add your Metahub server URL, so the tests will know which server to connect to.

## Setup

To run these tests, you will need the following:
* Any OS type
* Node.js 8.11.4 or later

Run:

```bash
$ npm install
```

## Running the tests with an existing Metahub server (All in one)

To run all the tests at once, use the command `npx codeceptjs run`. Please, Note that not all tests will be executed with success. Visual regression tests needs `--config=codecept.webdriver.conf.js`

## Running tests with an existing Metahub server (Specific test)

To run tests you can use:

```bash
$ npx codeceptjs run any_test.js
```

see: https://codecept.io/commands/#run-multiple to more examples of how to execute tests

## To run with firefox browser
First you need to execute: `PUPPETEER_PRODUCT=firefox npm i puppeteer` and change `.env` file to `BROWSER=firefox`

ATTENTION: It's not possible to change the browser only changing .env file. If you wish to go back to chrome you need to set `PUPPETEER_PRODUCT=chrome npm i puppeteer` and `.env` file to `BROWSER=chrome`

## Debugging, Recording, Evidences, Metrics and Performance

### Debugging

To debug the tests, you will need to set `DEBUG=true`; if `DEBUG` receives `true`, the logs will show in the console from which we start the tests. Also, you can pass `--debug` when run a test. 

see: https://codecept.io/commands/#run-multiple

### Recording

To record the tests, you will need to set `WITH_RECORD=true`; if `WITH_RECORD` receives `true`, all tests will be recorded to .mp4 files, to keep track on what's going on and to have a good approach to catch problems or to have a proof that there are no problems.

Recording output will be saved under `data/test-date-testName/recording`; for example: `data/test-19-01-2021-loginWithSuccess/recording`.

**PS:** Recordings are just for manual testing.

### Getting Evidences

Generating evidences is about to take screenshots of the client during testing. And to realize this, assigning `GENERATE_EVIDENCES` in `.env` to `true`. This will take screenshots and save them in `./output`; for example: `test-19-01-2021-loginWithSuccess/screenshots`.

### Visual Regression

Our test suite includes visual regression specs that can be execute separately with `npx codeceptjs run --grep "@visual-regression" --config=codecept.webdriver.conf.js` (desktop only). It will take screenshots of various views and components of the client or failed cases and save them inside the `./output` folder. Visual regression uses as base folder `./support/screenshots/base/` and diff folder `./support/screenshots/diff/`.

### Metrics

Setting `COLLECT_METRICS` to `true` generates page metrics like the following:

```
    "metricObj": {
        "Timestamp": 508539.87856,
        "Documents": 4,
        "Frames": 1,
        "JSEventListeners": 29,
        "Nodes": 198,
        "LayoutCount": 5,
        "RecalcStyleCount": 6,
        "LayoutDuration": 0.003339,
        "RecalcStyleDuration": 0.017187,
        "ScriptDuration": 0.252291,
        "TaskDuration": 0.685394,
        "JSHeapUsedSize": 15774440,
        "JSHeapTotalSize": 22032384
    },
```

This metrics report will be generated inside `data/test-date-testName/metrics/metrics-date.json` file.

### Performance

Setting `COLLECT_PERFORMANCE` to `true` generates page performance like the following:

```
    "performanceTimingObj": {
        "connectStart": 1640199656657,
        "navigationStart": 1640199656649,
        "loadEventEnd": 0,
        "domLoading": 1640199657115,
        "secureConnectionStart": 0,
        "fetchStart": 1640199656649,
        "domContentLoadedEventStart": 1640199670498,
        "responseStart": 1640199657109,
        "responseEnd": 1640199657111,
        "domInteractive": 1640199670498,
        "domainLookupEnd": 1640199656657,
        "redirectStart": 0,
        "requestStart": 1640199656881,
        "unloadEventEnd": 0,
        "unloadEventStart": 0,
        "domComplete": 0,
        "domainLookupStart": 1640199656657,
        "loadEventStart": 0,
        "domContentLoadedEventEnd": 1640199670498,
        "redirectEnd": 0,
        "connectEnd": 1640199656881
    }
```

This Performance report will be generated inside `data/test-date-testName/metrics/metrics-date.json` file.

```

## Running Allure Reports

To run reports:

```bash
$ npx codeceptjs run --grep @login --plugins allure

or

$ npx codeceptjs run --grep @all --plugins allure

```
In local environment, go to main directory project path and execute the command 
to see the results:
```bash
$ allure serve target/allure-results/

```
