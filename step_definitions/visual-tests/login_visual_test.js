
Feature('login visual-test');

Scenario('Verify the Login Screen @visual-regression', ({ I, LoginPage }) => {
    LoginPage.accessLoginPage();
    I.amOnPage('/');
    I.saveScreenshot('Login_Page_Image.png');
    I.seeVisualDiff('Login_Page_Image.png', { tolerance: 2, prepareBaseImage: false });
});
