const { I, DataSourcePage, UserPage } = inject();
const timeout = 20;
const assert = require('assert');
const FIELD_VALUE = '<VALUE>';

//#region .: Creating a new Team with success :.

Given('I am already in the Metahub teams page', async () => {
	await UserPage.click(UserPage.teamsMenu);
});

When('I fill the field team Name with the value {string}', async (roleName) => {
	await UserPage.fillField(UserPage.inputRoleName, roleName);
});

When('I select the user {string}', async (user) => {
	await UserPage.click("//INPUT[@class='bp3-input-ghost bp3-multi-select-tag-input-input']");
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, user), timeout);
});

Then('I can see the teams informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.NAME, row.DESCRIPTION, row.NUMBER_OF_MEMBERS , row.MEMBERS);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Trying create new team using invalid team name :.

Then('I can see the field error message {string}',async (message)=>{
	await I.waitForElement(UserPage.fieldErrorMessage);
	const value = await I.grabTextFrom(UserPage.fieldErrorMessage);
	resultValue = value.trim();
	assert.equal(resultValue,message);
})

//#endregion
