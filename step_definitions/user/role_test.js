const { I, DataSourcePage, UserPage, HomePage, MetahubApi } = inject();
const timeout = 20;
const assert = require('assert');
const FIELD_VALUE = '<VALUE>';


//Creating User By API

Given('I already have created the user {string} by api', async (userID) => {
	await MetahubApi.getSessionToken();
	const createNewUserResponse = await MetahubApi.createNewUser(userID);
	assert.equal(createNewUserResponse.status, 200);
});

//#endregion

//#region .: Creating a new role with success :.


Given('I am already in the Metahub roles page', async () => {
	await UserPage.click(UserPage.rolesMenu);
});

When('I fill the field Role Name with the value {string}', async (roleName) => {
	await UserPage.fillField(UserPage.inputRoleName, roleName);
});

When('I fill the text area Description with the value {string}', async (roleName) => {
	await UserPage.fillField(UserPage.textAreaDescription, roleName);
});

When('I select the permission {string}', async (permission) => {
	await UserPage.click(UserPage.checkBoxRolePermission.replace(FIELD_VALUE, permission), timeout);
});

When('I filter the role by the Name {string}', async (roleName) => {
	await UserPage.fillField(UserPage.inputSearchRole, roleName);
});

Then('I can see the role informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
		const fields = new Array(row.NAME, row.DESCRIPTION, row.PERMISSIONS);
		fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Checking permissions of the new user :.

When('I can see only the menu Idossier', async () => {
	await I.waitForElement(HomePage.idossierMenu);
	await I.dontSeeElement(HomePage.peopleMenu);
	await I.dontSeeElement(HomePage.analysisMenu);
	await I.dontSeeElement(HomePage.monitoringMenu);
	await I.dontSeeElement(HomePage.dashboardMenu);
	await I.dontSeeElement(HomePage.importMenu);
});

Then('I cannot see the button {string}', async (button) => {
	await I.dontSeeElement(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout)
});

Then('I delete the user {string} by api', async (user) => {
	await MetahubApi.getSessionToken();
	const deleteResponse = await MetahubApi.deleteUser(user);
	assert.equal(deleteResponse.status, 200);
});

//#endregion

//#region .: Trying create new role with invalid role name :.

Then("I can see user page error message {string}", async (message)=>{
	await I.waitForElement(UserPage.errorMessage);
	const value = await I.grabTextFrom(UserPage.errorMessage);
	resultValue = value.trim();
	assert.equal(resultValue,message);
})

//#endregion

//#region .: Trying edit role name to name already exists and delete the role by api :.

Then('I delete the test role by api', async ()=>{
	await MetahubApi.getSessionToken();
	const roleGetResponse = await MetahubApi.getRoles();
	console.log(roleGetResponse);
	listOfRoles= roleGetResponse.data.data;
	resultRole = listOfRoles.filter(x=> x.description=="Description test")[0];
	roleId = resultRole.id;
	const roleDeleteResponse = await MetahubApi.deleteRole(roleId);
	assert.equal(roleDeleteResponse.status,200);
})

//#endregion
