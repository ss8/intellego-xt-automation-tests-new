
const { I, LoginPage, DataSourcePage, UserPage } = inject();
const timeout = 20;
const assert = require('assert');
const FIELD_VALUE = '<VALUE>';


//#region .: Creating new user with success :.
Given('I am logged in metahub with adm user', async () => {
	await LoginPage.accessLoginPage();
	await LoginPage.fillUserAndPass(process.env.METAHUB_ADM_USER, process.env.METAHUB_ADM_PASS);
	await LoginPage.clickLogin();
});

Given('I am already in the Metahub users page', async () => {
    await UserPage.isCurrentPage();
});

When('I fill the field User with the value {string}', async (userInput) => {
	await UserPage.fillField(UserPage.inputNameUser, userInput);
});

When('I fill the field Email with the value {string}', async (emailInput) => {
	await UserPage.fillField(UserPage.inputEmailUser, emailInput);
});

When('I fill the field First Name with the value {string}', async (firstNameInput) => {
	await UserPage.fillField(UserPage.inputFirstName, firstNameInput);
});

When('I fill the field Last Name with the value {string}', async (lastNameInput) => {
	await UserPage.fillField(UserPage.inputLastName, lastNameInput);
});

When('I fill the field Role with the values {string}', async (role) => {
    await UserPage.click(UserPage.inputRoleField);
	role.split(',').forEach(async (item) => {
		await UserPage.click(UserPage.SelectOptionField.replace(FIELD_VALUE, item), timeout)     
	});
});

When('I fill the field Password with the value {string}', async (password) => {
	await UserPage.fillField(UserPage.inputPasswordField, password);
});

Then('I can see the user informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.USER_ID, row.EMAIL, row.FIRST_NAME , row.LAST_NAME, row.STATE);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Updating password on the first access :.

Given('I fill the field Login with {string} and Password with {string}', async (user, password) => {
	await LoginPage.fillUserAndPass(user, password);
});

When('I fill the field Enter Current Password with the value {string}', async (password) => {
	await UserPage.fillField(UserPage.inputPasswordField, password);
});

When('I fill the field Enter New Password', async () => {
	await UserPage.fillField(UserPage.inputNewPasswordField, process.env.METAHUB_PASS);
});

When('I fill the field Confirm New Password', async () => {
	await UserPage.fillField(UserPage.inputConfirmNewPasswordField, process.env.METAHUB_PASS);
});

When('I click in the button Submit', async () => {
	await UserPage.click(UserPage.submitButton);
});

Then('I click on the profile image with the initials {string}', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout);
});

Then('I can see the profile information {string}', async (userName) => {
	let value = await I.grabTextFrom(UserPage.userProfileInformation);
    assert.equal(value, userName);
});

//#endregion

//#region .: Edit and deactivate user with success :.

When('I already have a user with {string} name', async (userName) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, userName), timeout);
});

When('I filter the user by the name {string}', async (userName) => {
	await UserPage.fillField(UserPage.inputUserSearchField, userName);
	I.pressKey('Enter');
});

//#endregion

//#region .: Deleting iDossier data with success :.

Given('I fill the field Login with {string} and Password', async (user) => {
	await LoginPage.fillUserAndPass(user, process.env.METAHUB_PASS);
});

Then('I can see the error message {string}', async (message) => {
	await I.waitForElement(LoginPage.errorMessageLogin);
	const value = await I.grabTextFrom(LoginPage.errorMessageLogin);
	resultValue = value.trim();
	assert.equal(resultValue, message);
});

Then('I cannot see a user with the name {string}', async (userName) => {
	await I.dontSeeElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, userName), timeout);
});

//#endregion

//#region .: Trying create new user using password without one uppercase :.

Then("I can see the error password message {string}", async (message)=>{
	await I.waitForElement(UserPage.errorMessage);
	const value = await I.grabTextFrom(UserPage.errorMessage);
	resultValue = value.trim();
	assert.equal(resultValue,message);
})

//#endregion
