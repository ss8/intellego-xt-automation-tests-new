const { I, LoginPage, HomePage, DataSourcePage, IdossierPage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';

//#region .: Importing a new iDossier data source file with success :

When('I click in the {string} menu', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout);
});

When('I click in the {string} button', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout);
});

When('I select the iDossier csv file to be imported', async () => {
	const filepath = './support/files/test_idossier.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});


When('I select the option {string} in the Information Type column and {string} in the column Category at the line {string}', async (text, category, line) => {
    //Category Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceCategoryField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCategoryField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, category), timeout);
    //Information Type Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceFieldType.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceFieldType.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, text), timeout);
});

When('I select the option {string} in the Data Type column and {string} in the column Information Type and {string} in the column Category at the line {string}', async (text, type, category, line) => {
	//Data Type Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, text), timeout);
    //Information Type Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceFieldType.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceFieldType.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, type), timeout);
    //Category Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceCategoryField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCategoryField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, category), timeout);
});

Then('I can see the idossier file informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.DATA_SOURCE_NAME, row.DESCRIPTION, row.CREATED_BY);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Importing new data to iDossier with image :.

When('I click in the option {string}', async (importButton) => {
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, importButton), timeout);
});

When('I fill the field Import Name with the value {string}', async (importName) => {
	await DataSourcePage.fillField(DataSourcePage.importImputNameDataSource, importName);
});

When('I select the image file to be imported', async () => {
	const filepath = './support/files/images.zip';
	await DataSourcePage.attachFile(DataSourcePage.uploadImageZipFile, filepath);
});

Then('I can see the importation results:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.IMPORTED_FILE_NAME, row.IMPORTED_FILE_NAME_SECOND_LINE);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Consulting the data in the iDossier pages :.

Given('I am already in the iDossier page', async () => {
	await IdossierPage.click(IdossierPage.idossierMenuButton);
	await IdossierPage.isCurrentPage();
});

Then('I can see the iDossier data:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.NAME_LINE_1, row.NAME_LINE_2, row.NAME_LINE_3, row.NAME_LINE_4, row.NAME_LINE_5);
	  fields.forEach(item => I.waitForElement(IdossierPage.idossierPageTable.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Deleting a Idossier Data Source file with success :.

When('I already have a file with {string} name imported', async (fileName) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, "iDossier Data Sources"), timeout);
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, fileName), timeout);
});

Then('I can read the text information {string}', async (message) => {
	await I.waitForElement(DataSourcePage.screenTableMessage.replace(FIELD_VALUE, message), timeout);
});

Then('I click in the refresh option', async () => {
	await DataSourcePage.click(DataSourcePage.buttonRefresh);
	I.refreshPage(); //Will be removed after bug correction.
});

//#endregion
