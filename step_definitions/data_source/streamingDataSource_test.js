const { I, DataSourcePage, AnalysisPage, StreamingApi } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';
const assert = require('assert');


When('I select the streaming JSON file to be imported', async () => {
	const filepath = './support/files/small-enriched-data-new-fields-2-ds.json';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the checkbox Allow streaming type', async () => {
    await DataSourcePage.click(DataSourcePage.allowStreamingCheckBox);
});

When('I send the import file by streaming api', async () => {
	await StreamingApi.postStreamingFileApi();
	await StreamingApi.postStreamingBodyUrl();
	assert.equal(bodyFilePostResponse.status, 200);
});

Then('I can see the stream file informations:', async (table) => {
	for (let index = 0; index < 10; index++) {
		await DataSourcePage.click(DataSourcePage.buttonRefresh);
		I.wait(5);
        let value = await I.grabTextFrom(DataSourcePage.tableStatusValue);
		if (value === 'Streaming Ingested') { break; }
	  }
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {

	  const fields = new Array(row.Status, row.User, row.Imported_File_Name, row.Total_Records);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});
