const { I, LoginPage, HomePage, DataSourcePage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';


When('I select the bsa file to be imported', async () => {
	const filepath = './support/files/Cell_VNP_v3_300.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);

});