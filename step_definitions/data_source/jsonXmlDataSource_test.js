const { I, DataSourcePage, AnalysisPage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';


//#region .: Importing a new data source event file with JSON format :.

When('I select the event JSON file to be imported', async () => {
	const filepath = './support/files/cdr_rand_vn_2.json';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the checkbox of the fields {string} and {string}', async (Lat, Long) => {
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, Lat), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, Long), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, Long), timeout);
});

Then('I can see the file information:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {

	  const fields = new Array(row.DATA_SOURCE_NAME, row.FILE_TYPE, row.CREATED_BY, row.TOTAL_RECORDS, row.FAILED_RECORDS, row.TOTAL_IMPORTS);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Importing new data with a JSON file :.

When('I select the initial date {string} and the end date {string}', async (initialDate, endDate) => {
    let initialDateSplit = initialDate.split('/');
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.selectOption(AnalysisPage.selectMonthOption, initialDateSplit[1]);
	await AnalysisPage.click(AnalysisPage.selectMonthButton)
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.selectOption(AnalysisPage.selectYearOption, initialDateSplit[2]);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, initialDateSplit[0]), timeout);
//
    let endDateSplit = endDate.split('/');
    await AnalysisPage.click(AnalysisPage.selectEndMonthButton);
    await AnalysisPage.selectOption(AnalysisPage.selectMonthOption, endDateSplit[1]);
    await AnalysisPage.click(AnalysisPage.selectEndMonthButton)
    await AnalysisPage.click(AnalysisPage.selectEndYearButton);
    await AnalysisPage.selectOption(AnalysisPage.selectYearOption, endDateSplit[2]);
    await AnalysisPage.click(AnalysisPage.selectEndYearButton);
    await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, endDateSplit[0]), timeout);
});

//#endregion

//#region .: Importing a new data source event file with XMl format :.

When('I select the event XML file to be imported', async () => {
	const filepath = './support/files/anpr_test.xml';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

//#endregion

//#region .: Consulting imported JSON information in analysis page with advanced query :.

When('I select the option Advanced Query', async () => {
    await AnalysisPage.click(AnalysisPage.advancedQueryButton);
});

When('I fill the field Advanced Query with the value {string}', async (queryValue) => {
	await AnalysisPage.fillField(AnalysisPage.imputAdvancedQueryField, queryValue);
});

When('I click in the advanced search button', async () => {
	await AnalysisPage.click(AnalysisPage.analysisAdvancedSearchButton);
});

//#endregion
