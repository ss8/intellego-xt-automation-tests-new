
const { I, LoginPage, HomePage, DataSourcePage, AnalysisPage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';


//#region .: Importing a new data source CDR file using the BSA data source information with success :.

Given('I am logged in metahub', async () => {
	await LoginPage.accessLoginPage();
	await LoginPage.fillUserAndPass(process.env.METAHUB_USER, process.env.METAHUB_PASS);
	await LoginPage.clickLogin();
});

Given('I am already in the import page', async () => {
	await DataSourcePage.click(DataSourcePage.importMenu);
});

When('I click in the New Data Source button', async () => {
	await DataSourcePage.click(DataSourcePage.newDataSourceButton);
});

When('I fill the field Data Source Name with the value {string}', async (dataSourceInput) => {
	await DataSourcePage.fillField(DataSourcePage.dataSourceNameImput, dataSourceInput);
});

When('I fill the field Data Source Description with the value {string}', async (dataSourceDescription) => {
	await DataSourcePage.fillField(DataSourcePage.dataSourceDescription, dataSourceDescription);
});

When('I select the option {string} in the Data Source Type field', async (dataSourceType) => {
	await DataSourcePage.selectOption(DataSourcePage.dataSourceTypeDropDown, dataSourceType);
});

When('I select the option {string} in the Event Type field', async (eventType) => {
	await DataSourcePage.click(DataSourcePage.eventTypeDropDown);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, eventType), timeout);
});

When('I select the event csv file to be imported', async () => {
	const filepath = './support/files/cdr_2022-06-15_18-40-47-603415.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with {string} Field Name', async (dataType, mappedField, line) => {
	//Data Type Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, dataType), timeout);
	//Mapped Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, mappedField), timeout);
});

When('I select the option {string} in the column Data Type and {string} in the column Mapped Field, both in the line with A_SUBS Field Name', async (dataType, mappedField) => {
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, mappedField), timeout);
	//Data Type Field
	await DataSourcePage.click(DataSourcePage.tableDataSourceDataTypedField.replace(FIELD_VALUE, mappedField), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, dataType), timeout);
	//Mapped Field
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, mappedField), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, mappedField), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, mappedField), timeout);
});

When('I select the option {string} in the column Map As in the line with {string} Field Name', async (map, line) => {
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceMapAsdField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceMapAsdField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, map), timeout);
});

When('I select the checkbox of the fields {string}, {string}, {string} and {string}', async (MCC, MNC, LAC, CELLID) => {
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, MCC), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, MNC), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, LAC), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, CELLID), timeout);
});

When('I click in the button {string}', async (button) => {
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout);
});

Then('I can see the toast message {string}', async (message) => {
	await I.waitForElement(DataSourcePage.toastMessage.replace(FIELD_VALUE, message), timeout);
});

Then('I can see the file informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.DATA_SOURCE_NAME, row.DESCRIPTION, row.CREATED_BY , row.TOTAL_RECORDS, row.FAILED_RECORDS, row.FAILED_RECORDS, row.TOTAL_IMPORTS);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

//#endregion

//#region .: Consulting imported information in analysis page :.

Given('I am already in the analysis page', async () => {
	await AnalysisPage.click(AnalysisPage.analysisMenu);
});

When('I click on the calendar', async () => {
	await AnalysisPage.click(AnalysisPage.buttonCalendar);
});

When('I select the initial date {string} until day {string}', async (initialDate, endDay) => {
    let dateSplit = initialDate.split('/');
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.selectOption(AnalysisPage.selectMonthOption, dateSplit[1]);
	await AnalysisPage.click(AnalysisPage.selectMonthButton)
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.selectOption(AnalysisPage.selectYearOption, dateSplit[2]);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, dateSplit[0]), timeout);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, endDay), timeout);
});

When('I click in the search button', async () => {
	await AnalysisPage.click(AnalysisPage.analysisSearchButton);
});

Then('I can see the total results {string}', async (results) => {
	await I.waitForText((AnalysisPage.resultsFilter, results), timeout);
});

//#endregion

//#region .: Consulting imported information in analysis page :.

When('I click on the option With Geolocation', async () => {
	await AnalysisPage.click(AnalysisPage.withGeolocationButton);
});

Then('I can see the total Geo Locations Found: {string}', async (results) => {
	await I.waitForText((AnalysisPage.resultsFilterGeoLocation, results), timeout);
});

//#endregion


//#region .: Deleting event data of the CDR data source :.

When('I fill the field Import Name with the {string} value', async (importName) => {
	await DataSourcePage.fillField(DataSourcePage.importNameDataSource, importName);
});

Then('I can see the informations:', async (table) => {
	for (let index = 0; index < 10; index++) {
		await DataSourcePage.click(DataSourcePage.buttonRefresh);
		I.wait(5);
        let value = await I.grabTextFrom(DataSourcePage.tableStatusValue);
		if (value === 'Ingested') { break; }
	  }
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {

	  const fields = new Array(row.Name, row.Status, row.User, row.Imported_File_Name, row.Total_Records);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

Then('I click in the refresh button', async () => {
	for (let index = 0; index < 300; index++) {
	    await DataSourcePage.click(DataSourcePage.buttonRefresh);
	    I.wait(5);
        let value = await I.grabNumberOfVisibleElements(DataSourcePage.tableStatusValue);
	    if (value === 0) { break; }
    }
});
	
Then('I can see the text information {string}', async (message) => {
	await I.waitForElement(DataSourcePage.screenTableMessage.replace(FIELD_VALUE, message), timeout);

	});

//#endregion

//#region .: Deleting a CDR Data Source file with success :.

When('I already have a file with {string} imported', async (fileName) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, fileName), timeout);
});

When('I click in the option {string}', async (viewButton) => {
	await I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, fileName), timeout);
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, viewButton), timeout);
});

When('I click in the trash button and confirm pressing the option {string}', async (yesOption) => {
	await DataSourcePage.click(DataSourcePage.buttonTrash);
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, yesOption), timeout);
});

When('I click in the option {string} and confirm pressing the option {string}', async (deleteButton, yesOption) => {
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, deleteButton), timeout);
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, yesOption), timeout);
});

//#endregion
