const { I, DataSourcePage, AnalysisPage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';


When('I select the file with no location to be imported', async () => {
	const filepath = './support/files/mini-linkchart.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});