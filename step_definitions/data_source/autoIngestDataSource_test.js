const { I, DataSourcePage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';
const { Client } = require('node-scp')


When('we copy a import file to the folder {string}', async (destinationFolder) => {
  async function scpImportDataSource() {
    const pathToFile = ("./support/streaming_file")
    const pathToNewDestination = (destinationFolder)
		try {
		  const client = await Client({
			host: process.env.SERVER_ADDRESS,
			port: 22,
			username:  process.env.SERVER_USER,
			password: process.env.SERVER_PASS,
		  })
		  await client.uploadDir(pathToFile, pathToNewDestination )
		  client.close()
		} catch (e) {
		  console.log(e)
		}
	  }
    await scpImportDataSource();
});

Then('I can see the auto ingested informations:', async (table) => {
	for (let index = 0; index < 10; index++) {
		await DataSourcePage.click(DataSourcePage.buttonRefresh);
		I.wait(5);
        let value = await I.grabTextFrom(DataSourcePage.tableStatusValue);
		if (value === 'Ingested') { break; }
	  }
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {
	  const fields = new Array(row.Status, row.User, row.Imported_File_Name, row.Total_Records);
	  fields.forEach(item => I.waitForElement(DataSourcePage.fieldTableDataSource.replace(FIELD_VALUE, item), timeout));
	}
});

Then('I click in the refresh button until the import is loaded', async () => {
	for (let index = 0; index < 300; index++) {
	    await DataSourcePage.click(DataSourcePage.buttonRefresh);
	    I.wait(5);
        let value = await I.grabNumberOfVisibleElements(DataSourcePage.tableStatusValue);
	    if (value !== 0) { break; }
    }
});
