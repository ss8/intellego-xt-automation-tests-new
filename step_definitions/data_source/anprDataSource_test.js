const { I, LoginPage, HomePage, DataSourcePage } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';

//#region .: Importing a new data source ANPR file with success using BSA data :

When('I select the ANPR csv file to be imported', async () => {
	const filepath = './support/files/anpr_small_2.csv';
	await DataSourcePage.attachFile(DataSourcePage.uploadDataSourceFile, filepath);
});

When('I select the option {string} in the Mapped Field column at the line with value {string}', async (name, line) => {
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceMappedField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, name), timeout);
});

When('I select the option {string} in the Map as column at the line with value {string}', async (name, line) => {
	await DataSourcePage.doubleClick(DataSourcePage.tableDataSourceMapAsdField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceMapAsdField.replace(FIELD_VALUE, line), timeout);
	await DataSourcePage.click(DataSourcePage.fieldValue.replace(FIELD_VALUE, name), timeout);
});

When('I unselect the checkbox in line with value {string}', async (line) => {
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, line), timeout);
});

When('I select the checkbox of the fields {string} and {string} and click in {string}', async (Lat, Long, button) => {
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, Lat), timeout);
	await DataSourcePage.click(DataSourcePage.tableDataSourceCheckbox.replace(FIELD_VALUE, Long), timeout);
	await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, button), timeout);
});

//#endregion
