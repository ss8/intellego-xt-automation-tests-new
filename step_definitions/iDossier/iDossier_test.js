const { I, IdossierPage, DataSourcePage } = inject();
var assert = require('assert');
const timeout = 30;
const FIELD_VALUE = '<VALUE>';


When('there is no Idossier information on the system', async () => {
    await IdossierPage.fillField(IdossierPage.inputCategory, "Basic");
    I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, "Delete"), timeout);
    await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, "Yes, delete it"), timeout);
    I.refreshPage();
    await IdossierPage.fillField(IdossierPage.inputCategory, "Mobile");
    I.pressKey('Enter');
	await DataSourcePage.click(DataSourcePage.threeDots);
	await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, "Delete"), timeout);
    await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, "Yes, delete it"), timeout);
    I.refreshPage();
});

//#region .: Creating new iDossier data with success :.

When('I fill the field Enter iDossier Name with the value {string}', async (idosseirInput) => {
    await IdossierPage.fillField(IdossierPage.idossierInputNameField, idosseirInput);
});

When('I fill the field Enter Description with the value {string}', async (idosseirInput) => {
    await IdossierPage.fillField(IdossierPage.idossierInputDescField, idosseirInput);
});

When('I select a profile image to upload', async () => {
    await IdossierPage.click(IdossierPage.uploadImageButton);
    const filepath = './support/files/profile_test.png';
	await IdossierPage.attachFile(IdossierPage.uploadFile, filepath);
});

When('I fill the field category with the value {string}', async (category) => {
    await IdossierPage.fillField(IdossierPage.inputCategory, category);
    I.pressKey('Enter');
});

When('I select the option {string}', async (option) => {
    await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, option), timeout)
});

When('I fill the field Description with the value {string}', async (idosseirInput) => {
    await IdossierPage.fillField(IdossierPage.inputCategory, idosseirInput);
});

When('I fill the field Mobile Phone with the value {string}', async (idosseirInput) => {
    await IdossierPage.fillField(IdossierPage.inputCategory, idosseirInput);
});


When('I fill the text area Description with the value {string}', async (idosseirInput) => {
    await IdossierPage.fillField(IdossierPage.textAreaDescription, idosseirInput);
});

Then('I can see the iDossier informations:', async (table) => {
	const tableByHeader = table.parse().hashes();
	for (const row of tableByHeader) {

	  const fields = new Array(row.NAME, row.DESCRIPTION, row.CREATED_BY, row.UPDATED_BY, row.CATEGORIES, row.CARDS);
	  fields.forEach(item => I.waitForElement(IdossierPage.idossierPageTable.replace(FIELD_VALUE, item), timeout));
   }
});

//#endregion

//#region .: Editing iDossier information using the edit option :.

When('I edit the infomation of the field iDossier name to {string}', async (idosseirName) => {
    await IdossierPage.fillField(IdossierPage.idossierEditName, idosseirName);
});

When('I edit the infomation of the field iDossier description to {string}', async (idosseirDescription) => {
    await IdossierPage.click(IdossierPage.idossierEditDescription);
    await IdossierPage.fillField(IdossierPage.idossierEditDescription, idosseirDescription);
});

//#endregion

//#region .: Editing iDossier information using the view option plus the edit button :.

When('I click in the first card', async () => {
    await IdossierPage.click(IdossierPage.idossierCardClick);
});

When('I edit the informations', async () => {
    await IdossierPage.fillField(IdossierPage.idossierEditCardDescription, 'Category description Edited');
    await IdossierPage.fillField(IdossierPage.textAreaDescription, 'Category text area description Edited');
});

Then('I can see the field Description with value {string}', async (descriptionValue) => {
    I.wait(5);
    await IdossierPage.click(DataSourcePage.threeDots);
    await DataSourcePage.click(DataSourcePage.SelectOptionField.replace(FIELD_VALUE, 'View'), timeout)
    await DataSourcePage.click(DataSourcePage.buttons.replace(FIELD_VALUE, 'Edit'), timeout);
    await IdossierPage.click(IdossierPage.idossierCardClick);
    let value = await I.grabValueFrom(IdossierPage.idossierEditCardDescription);
    assert.equal(value, descriptionValue);
});

Then('I can see the text area description with the value {string}', async (descriptionValue) => {
    let value = await I.grabValueFrom(IdossierPage.textAreaDescription);
    assert.equal(value, descriptionValue);
});

Then('I can see the text area description with the value {string}', async (descriptionValue) => {
    let value = await I.grabValueFrom(IdossierPage.textAreaDescription);
    assert.equal(value, descriptionValue);
});

//#endregion

//#region .: Deleting iDossier data with success :.

Then('I already have the iDossier with {string} name in the system', async (name) => {
	await I.waitForElement(IdossierPage.idossierPageTable.replace(FIELD_VALUE, name), timeout);
});

Then('I cannot see the iDossier with {string} name in the system', async (name) => {
    I.refreshPage();
	await I.dontSeeElement(IdossierPage.idossierPageTable.replace(FIELD_VALUE, name), timeout);
});

//#endregion
