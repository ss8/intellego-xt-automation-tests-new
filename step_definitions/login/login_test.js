const { I, LoginPage, HomePage, AdmHomePage } = inject();
const timeout = 20;
const assert = require('assert');

//#region .: Realize login and logout to Metahub :.

Given('I am already in the Metahub login page', async () => {
	await LoginPage.accessLoginPage();
});

When('I fill the fields username and password', async () => {
	await LoginPage.fillUserAndPass(process.env.METAHUB_USER, process.env.METAHUB_PASS);
});

When('I click submit', async () => {
	await LoginPage.clickLogin();
});

When('I click menu button', async () => {
	I.waitForElement(HomePage.menuButton);
	await HomePage.click(HomePage.menuButton);
});

When('I click logout', async () => {
	I.waitForElement(HomePage.textOverflow.replace("<VALUE>","Logout"));
	await HomePage.click(HomePage.textOverflow.replace("<VALUE>","Logout"));
});

Then('I can see the Metahub Home Page', async () => {
	await HomePage.isHomePage();
});

Then('I logout of the metahub', async ()=>{
	LoginPage.isCurrentPage();
})

//#endregion

//Scenario outline: Wrong password or username

When('I fill the field Username with {string} and Password with {string}', async (username, password) => {
	await LoginPage.fillField(LoginPage.userInput, username);
	await LoginPage.fillField(LoginPage.passwordInput, password);
});

Then('I can see the message {string}', async (message) => {
	I.waitForElement(LoginPage.errorMessageLogin);
	const value = await I.grabTextFrom(LoginPage.errorMessageLogin);
	resultValue = value.trim();
	assert.equal(resultValue, message);
});

//#endregion

//#region .: Realize login and logout to Metahub with adm :.

When('I fill the fields with adm username and password', async () => {
	await LoginPage.fillUserAndPass(process.env.METAHUB_ADM_USER, process.env.METAHUB_ADM_PASS);
});

Then('I can see the Adm Metahub Home Page', async () => {
	await AdmHomePage.isAdmHomePage();
});

//#endregion
