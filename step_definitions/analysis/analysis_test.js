const { I, LoginPage, HomePage, AnalysisPage, MetahubApi } = inject();
const timeout = 20;
const FIELD_VALUE = '<VALUE>';
const assert = require('assert');

//#region .: Sample query :

Given('I am already in the Metahub analysis page', async () => {
	await AnalysisPage.click(AnalysisPage.analysisMenu);
});

Given('I already added data source by api', async ()=>{
	await MetahubApi.getSessionTokenWithQAUser();
	await MetahubApi.createNewDataSource();
})

Given('I already added import to data source by api', async ()=>{
	await MetahubApi.getSessionTokenWithQAUser();
	jsonFile = await MetahubApi.generateFileWithActualDate();
	dataSourceGetResponse= await MetahubApi.getDataSources();
	dataSourceID = dataSourceGetResponse.data.data[0].id;
	res = await MetahubApi.postImportFile(dataSourceID,jsonFile);
	assert.equal(res.status,200);
})

When('I click in the search button',async ()=>{
	await AnalysisPage.click(AnalysisPage.analysisSearchButton);
})

When('I click in the date button {string}', async (value)=>{
	I.waitForElement(HomePage.textOverflow.replace("<VALUE>",value));
	await HomePage.click(HomePage.textOverflow.replace("<VALUE>",value));
})

Then('I delete import of data source by api', async ()=>{
	await MetahubApi.getSessionTokenWithQAUser();
	let dataImportGetAllResponse = await MetahubApi.getAllImportDataSources();
	let importId = dataImportGetAllResponse.data.data[0].id;
	res = await MetahubApi.deleteImportFile(importId);
	assert.equal(res.status,200)
})

Then('I wait import are deleted', async ()=>{
	await MetahubApi.getSessionTokenWithQAUser();
	dataSourceGetResponse= await MetahubApi.getDataSources();
	canDelete = dataSourceGetResponse.data.data[0].canDelete;
	for (let index = 0; index < 300; index++) {
	    I.wait(5);
        if (canDelete) { break; }
		dataSourceGetResponse= await MetahubApi.getDataSources();
		canDelete = dataSourceGetResponse.data.data[0].canDelete;
    }
})

Then('I delete data source by api', async ()=>{
	await MetahubApi.getSessionTokenWithQAUser();
	dataSourceGetResponse= await MetahubApi.getDataSources();
	dataSourceID = dataSourceGetResponse.data.data[0].id;
	res = await MetahubApi.deleteDataSourceFile(dataSourceID);
	console.log(res);
	assert.equal(res.status,200)
})

Then('Analysis results are equal {string}', async (number)=>{
	I.wait(5);
	I.waitForElement(AnalysisPage.resultsFilter);
	const value =  await I.grabTextFrom(AnalysisPage.resultsFilter);
	let resultValue = value.trim();
	console.log(resultValue);
	assert.equal(number,resultValue);
})

//#endregion

//#region .: Alternative sample query :

Given('The calendar is already open',async ()=>{
	await AnalysisPage.click(AnalysisPage.buttonCalendar);
})

When('I select only initial date {string}', async (initialDate)=>{
	let initialDateSplit = initialDate.split('/');
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.selectOption(AnalysisPage.selectMonthOption, initialDateSplit[1]);
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.selectOption(AnalysisPage.selectYearOption, initialDateSplit[2]);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, initialDateSplit[0]), timeout);
})

When('I select only end date {string}',async (endDate)=>{
	let endDateSplit = endDate.split('/');
	let initialDateSplit = ["1",endDate[1],endDate[2]];
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.selectOption(AnalysisPage.selectMonthOption, initialDateSplit[1]);
	await AnalysisPage.click(AnalysisPage.selectMonthButton);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.selectOption(AnalysisPage.selectYearOption, initialDateSplit[2]);
	await AnalysisPage.click(AnalysisPage.selectYearButton);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, initialDateSplit[0]), timeout);
	await AnalysisPage.click(AnalysisPage.selectDay.replace(FIELD_VALUE, endDateSplit[0]), timeout);
	await AnalysisPage.click(AnalysisPage.unselectDay.replace(FIELD_VALUE, initialDateSplit[0]), timeout);
})

Then('I can see the search button is disabled', async ()=>{
	I.waitForElement(AnalysisPage.disabledSearchButton);
})

//#endregion
