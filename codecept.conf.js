const dotenv = require('dotenv');
const dotenvParseVariables = require('dotenv-parse-variables');
const ls = require('local-storage');
const axios = require('axios');

let env = dotenv.config()
if (env.error) throw env.error;
env = dotenvParseVariables(env.parsed);

exports.config = {
	output: './output',
	helpers: {
		Puppeteer: {
			url: process.env.BASE_URL,
			show: process.env.SHOW_BROWSER === 'true' ? true : false,
			browser: process.env.BROWSER,
			windowSize: process.env.WINDOW_SIZE,
			waitForNavigation: "networkidle0",
			getPageTimeout: 0,
			waitForAction: 700,
			chrome: {
				args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-dev-shm-usage',
					'--ignore-certificate-errors']
			},
			firefox: {
				args: ['--no-sandbox', '--ignore-certificate-errors']
			}
		},
		CustomHelper: {
			require: './support/helpers/custom_helper.js',
		},
		REST: {
			endpoint: process.env.BASE_URL+'metahub/api/',
			
		}
	},
	include: {
		I: './steps_file.js',
		LoginPage: './page_objects/login_page.js',
		HomePage: './page_objects/home_page.js',
		AdmHomePage: './page_objects/adm_home_page.js',
		DataSourcePage: './page_objects/dataSource_page.js',
		IdossierPage: './page_objects/idossier_page.js',
		AnalysisPage: './page_objects/analysis_page.js',
		UserPage: './page_objects/user_page.js',
		MetahubApi: './page_objects/metahub_api.js',
		StreamingApi: './page_objects/streaming_api.js'
	},
	async bootstrapAll() {
		{
			const postEndpointUrlAuth = (process.env.BASE_URL + 'metahub/api/v1/users/authenticate');
			const authPostResponse = await axios.post(postEndpointUrlAuth, { "user": process.env.METAHUB_ADM_USER, "password": process.env.METAHUB_ADM_PASS });
			ls('x-auth-token', authPostResponse.headers['x-auth-token']);
			const postEndpointUrlUsers = (process.env.BASE_URL + 'metahub/api/v1/users');
			await axios.post(postEndpointUrlUsers, {
				"userID": 'qa',
				"password": "Test123456!",
				"role": "",
				"firstName": "",
				"middleName": "",
				"lastName": "",
				"displayName": "qa",
				"office": "",
				"dept": "",
				"email": "",
				"phone": "",
				"state": "active",
				"superior": 0,
				"validLicenses": [
				],
				"privileges": [
				],
				"reporting": {
					"assignedUsers": [
						{
							"id": "dolor cup",
							"name": "qui sit commodo mollit"
						},
						{
							"id": "officia mollit reprehenderit ut",
							"name": "nostrud commodo veniam eu"
						}
					]
				},
				"intellegoUser": false
			},
				{
					headers: {
						'x-auth-token': ls.get('x-auth-token'),
						'Content-Type': 'application/json'
					},
				},
			).catch(function (response) {
				console.log(response);
			});
			const postEndpointUrlRole = (process.env.BASE_URL + 'metahub/api/v1/users/qa/roles');
			await axios.post(postEndpointUrlRole,
				["Manage Users",
					"Manage Teams",
					"Manage Roles",
					"Manage Alarms",
					"Manage Audit Log",
					"Manage Configuration",
					"Manage Dashboards/Reports",
					"Manage DataSource and Data Imports",
					"Manage Query Permissions",
					"Manage Idossier Permissions",
					"Manage Notifications",
					"Manage Area Permissions"],
				{
					headers: {
						'x-auth-token': ls.get('x-auth-token'),
						'Content-Type': 'application/json'
					},

				},
			).catch(function (response) {
				console.log(response);
			});
			const postEndpointUrlIdossier = (process.env.BASE_URL + 'metahub/api/v1/idossier');
			await axios.post(postEndpointUrlIdossier,
				{
					"id": "",
					"createdBy": "qa",
					"contents": {
						"lanes": [
							{
								"id": "8c67db50-f3cb-11ec-8091-bddd05b202f0",
								"cards": [],
								"title": "Basic Info"
							}
						]
					},
					"description": "Basic Info",
					"type": 1,
					"name": "Basic Info",
					"profileImage": ""
				},
				{
					headers: {
						'x-auth-token': ls.get('x-auth-token'),
						'Content-Type': 'application/json'
					},
				},
			).catch(function (response) {
				console.log(response);
			});
			await axios.post(postEndpointUrlIdossier,
				{
					"id": "",
					"createdBy": "qa",
					"contents": {
						"lanes": [
							{
								"id": "8c67db50-f3cb-11ec-8091-bddd05b202f0",
								"cards": [],
								"title": "Mobile Phone"
							}
						]
					},
					"description": "Mobile Phone",
					"type": 1,
					"name": "Mobile Phone",
					"profileImage": ""
				},
				{
					headers: {
						'x-auth-token': ls.get('x-auth-token'),
						'Content-Type': 'application/json'
					},
				},
			).catch(function (response) {
				console.log(response);
			});
			const postEndpointUrlEventtypes = (process.env.BASE_URL + 'metahub/api/v1/ingestdatasource/eventtypes');
			await axios.post(postEndpointUrlEventtypes,
				{"eventType":"ANPR","id":0},
				{
					headers: {
						'x-auth-token': ls.get('x-auth-token'),
						'Content-Type': 'application/json'
					},
				},
			).catch(function (response) {
				console.log(response);
			})
		}
	},
	async teardownAll() {
		const postEndpointUrlAuth = (process.env.BASE_URL + 'metahub/api/v1/users/authenticate');
		const authPostResponse = await axios.post(postEndpointUrlAuth, { "user": process.env.METAHUB_ADM_USER, "password": process.env.METAHUB_ADM_PASS });
		ls('x-auth-token', authPostResponse.headers['x-auth-token']);
		const deleteEndpointUrlUsers = (process.env.BASE_URL + 'metahub/api/v1/users/qa');
		await axios.delete(deleteEndpointUrlUsers,
			{
				headers: {
					'x-auth-token': ls.get('x-auth-token'),
					'Content-Type': 'application/json'
				},
			},
		).catch(function (response) {
			console.log(response);
		})
	},
	mocha: {},
	bootstrap: null,
	teardown: null,
	hooks: [],
	gherkin: {
		features: './features/*/*.feature'
	},
	plugins: {
		allure: {
			enabled: true,
            require: '@codeceptjs/allure-legacy',
            outputDir: 'target/allure-results'
		},
		screenshotOnFail: {
			enabled: true
		},
		skipper: {
			require: './support/plugins/skipper.js',
			enabled: true
		}
	},
	tests: './step_definitions/*/*_test.js',
	name: 'e2e'
}
